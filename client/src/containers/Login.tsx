import React from 'react';
import { LoginForm } from '../components/login/LoginForm';
import { Container, Divider } from 'semantic-ui-react';
import { authResource } from '../rest/Resources';
import { AuthenticationResponse } from '../types/AuthTypes';
import { TransitionContainer } from '../components/hoc/TrainsitionContainer';
import { useRest } from '../hooks/UseRest';
import { PreAppMenu } from '../components/menu/PreAppMenu';

interface Props {
    setAuthToken: (token: string) => void;
}
export const Login = (props: Props) => {
    const { setAuthToken } = props;

    const setToken = (response: AuthenticationResponse) => {
        setAuthToken(response.token);
    }

    const { execute: authenticate, isPending: isAuthPending } = useRest(authResource.authenticate, setToken, undefined, true);

    return (
        <React.Fragment>
            <PreAppMenu isLoginActive={true} />
            <Divider hidden />
            <TransitionContainer>
                <Container text={true}>
                    <LoginForm onSubmit={authenticate} isPending={isAuthPending} />
                </Container>
            </TransitionContainer>
        </React.Fragment>
    )
}