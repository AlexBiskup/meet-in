import React from 'react';
import { TransitionContainer } from '../components/hoc/TrainsitionContainer';
import { SignupForm } from '../components/signup/SignupForm';
import { Container, Divider } from 'semantic-ui-react';
import { signupResource } from '../rest/Resources';
import { RouterProps } from 'react-router';
import { notifySuccess } from '../utils/Notifications';
import { useRest } from '../hooks/UseRest';
import { PreAppMenu } from '../components/menu/PreAppMenu';

interface Props extends RouterProps {

}
export const Signup = (props: Props) => {

    const { history } = props;

    const afterSignup = () => {
        history.push('/')
        notifySuccess('Erfolgreich registriert')
    }

    const signup = useRest(signupResource.signup, afterSignup);

    return (
        <React.Fragment>
            <PreAppMenu isSignupActive={true} />
            <Divider hidden />
            <TransitionContainer>
                <Container text>
                    <SignupForm onSubmit={signup.execute} isPending={signup.isPending} />
                </Container>
            </TransitionContainer>
        </React.Fragment>
    )
}