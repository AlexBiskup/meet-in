import React from 'react';
import { ProjectReadDto, ProjectUpdateDto } from '../types/ApiTypes';
import { Container, Header, Divider } from 'semantic-ui-react';
import { ProjectForm } from '../components/projects/ProjectForm';
import { ParticipantForm } from '../components/participants/ParticipantForm';
import { useRest } from '../hooks/UseRest';
import { participantsResource } from '../rest/Resources';
import { Rest } from '../components/hoc/Rest';
import { notifySuccess } from '../utils/Notifications';
import { ParticipantList } from '../components/participants/ParticipantsList';

interface Props {
    project: ProjectReadDto
    onUpdateProject: (projectId: number, project: ProjectUpdateDto) => any;
    isUpdatePending: boolean;
}
export const Settings = (props: Props) => {
    const { project, onUpdateProject, isUpdatePending } = props;

    const getParticipants = useRest(participantsResource.getAll);

    const afterCreate = () => {
        notifySuccess('Der Teilnehmer wurde hinzugefügt');
        getParticipants.execute(project.id);
    }

    const afterDelete = () => {
        notifySuccess('Der Teilnehmer wurde gelöscht');
        getParticipants.execute(project.id);
    }

    const createParticipant = useRest(participantsResource.create, afterCreate);
    const deleteParticipant = useRest(participantsResource.deleteParticipant, afterDelete)

    return (
        <Container text>
            <Header as='h2' content='Projekt bearbeiten' />
            <Divider hidden />
            <ProjectForm
                project={{ name: project.name, autoDismiss: project.autoDismiss, autoFixing: project.autoFixing }}
                onSubmit={(updatedProject) => onUpdateProject(project.id, updatedProject)}
                isPending={isUpdatePending}
            />
            <Divider hidden />
            <Divider />
            <Header as='h2' content='Teilnehmer' />
            <Divider hidden />
            <ParticipantForm
                isPending={createParticipant.isPending}
                onSubmit={(participant) => createParticipant.execute(participant, project.id)}
            />
            <Divider hidden />
            <Rest {...getParticipants} executeOnMount={get => get(project.id)}>
                {(participants) => (
                    <ParticipantList participants={participants} onDelete={deleteParticipant.execute} />
                )}
            </Rest>
        </Container>
    )
}