import React from 'react';
import AppRoutes from '../routes/AppRoutes';
import PreAppRoutes from '../routes/PreAppRoutes';
import { useLocalStorage } from '../hooks/UseLocalStorage';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-datepicker/dist/react-datepicker.css";

const App = () => {

  const [authToken, setAuthToken, resetAuthToken] = useLocalStorage(AUTH_TOKEN_KEY);

  return (
    <React.Fragment>
      {authToken && (
        <LogoutContext.Provider value={resetAuthToken}>
          <AppRoutes logout={resetAuthToken} />
        </LogoutContext.Provider>
      )}
      {!authToken && (
        <PreAppRoutes setAuthToken={setAuthToken} />
      )}
      <ToastContainer />
    </React.Fragment>
  )
}

export default App;

export const AUTH_TOKEN_KEY = 'meet-in_authToken';

export const LogoutContext = React.createContext(() => { });


