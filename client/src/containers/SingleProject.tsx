import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { TransitionContainer } from '../components/hoc/TrainsitionContainer';
import { useRest } from '../hooks/UseRest';
import { projectsResource, appointmentsResource, votesResource } from '../rest/Resources';
import { Rest } from '../components/hoc/Rest';
import { NewAppointment } from '../components/appointments/NewAppointment';
import { RouterProps, Redirect } from 'react-router';
import { notifySuccess } from '../utils/Notifications';
import { ProjectHeader } from '../components/singleProject/ProjectHeader';
import { Appointments } from './Appointments';
import { Divider } from 'semantic-ui-react';
import { Settings } from './Settings';
import { Votes } from '../components/votes/Votes';
import { AppointmentDetails } from '../components/appointments/AppointmentDetails';
import { AppointmentVotes } from '../components/appointments/AppointmentVotes';

interface Props extends RouterProps {
    projectId: number
}
export const SingleProject = (props: Props) => {
    const { projectId, history } = props;

    const afterUpdateProject = () => {
        getProject.execute(projectId);
        notifySuccess('Die Änderungen wurden gespeichert');
    }

    const afterCreateAppointment = () => {
        history.goBack();
        notifySuccess('Der Termin wurde erstellt');
    }

    const getProject = useRest(projectsResource.getById);
    const updateProject = useRest(projectsResource.update, afterUpdateProject)

    const createAppointment = useRest(appointmentsResource.create, afterCreateAppointment);


    return (
        <Rest {...getProject} executeOnMount={get => get(projectId)}>
            {(project) => (
                <TransitionContainer>
                    <BrowserRouter basename={`/projects/${projectId}`}>
                        <ProjectHeader project={project} />
                        <Divider hidden />
                        <Divider hidden />
                        <Switch>
                            <Route path='/appointments' exact render={(props) => <AppointmentsRestWrapper isOwner={project.participantIsOwner} projectId={projectId} {...props} />} />
                            <Route path='/votes' render={() => <VotesRestWrapper projectId={projectId} />} />
                            <Route path='/appointments/:appointmentId/votes'>
                                {({ match, history }) => (
                                    <AppointmentVotes onClose={history.goBack} appointmentId={match && match.params.appointmentId} />
                                )}
                            </Route>
                            {project.participantIsOwner && (
                                <Switch>
                                    <Route path='/settings'>
                                        {() => (
                                            <Settings
                                                project={project}
                                                isUpdatePending={updateProject.isPending}
                                                onUpdateProject={updateProject.execute}
                                            />
                                        )}
                                    </Route>
                                    <Route path='/new'>
                                        {({ history }) => (
                                            <NewAppointment
                                                projectId={projectId}
                                                isCreationPending={createAppointment.isPending}
                                                onClose={history.goBack}
                                                onCreate={createAppointment.execute}
                                            />
                                        )}
                                    </Route>
                                    <Route path='/appointments/:appointmentId'>
                                        {({ match, history }) => (
                                            <AppointmentDetails onClose={history.goBack} appointmentId={match && match.params.appointmentId} />
                                        )}
                                    </Route>
                                    <Redirect to='/appointments' />
                                </Switch>
                            )}
                            <Redirect to='/appointments' />
                        </Switch>
                    </BrowserRouter>
                </TransitionContainer>
            )}
        </Rest>
    )
}

interface ARWProps extends RouterProps {
    projectId: number;
    isOwner: boolean;
}
const AppointmentsRestWrapper = (props: ARWProps) => {

    const { projectId, isOwner, ...rest } = props;

    const getAppointments = useRest(appointmentsResource.getAll);
    const updateAppointment = useRest(appointmentsResource.update);

    return (
        <Rest {...getAppointments} executeOnMount={get => get(projectId)}>
            {(appointments) => (
                <Appointments
                    appointments={appointments}
                    onUpdate={updateAppointment.execute}
                    isStatusChangeEnabled={isOwner}
                    {...rest}
                />
            )}
        </Rest>
    )
}

interface VRWProps {
    projectId: number;
}
const VotesRestWrapper = (props: VRWProps) => {
    const { projectId } = props;
    const getVotes = useRest(votesResource.getAll);
    const updateVote = useRest(votesResource.update);

    return (
        <Rest {...getVotes} executeOnMount={get => get(projectId)}>
            {(votes) => (
                <Votes votes={votes} onUpdate={updateVote.execute} />
            )}
        </Rest>
    )
}

