import React, { useState } from 'react';
import { DragDropContext, DropResult } from 'react-beautiful-dnd';
import { Grid } from 'semantic-ui-react';
import { DroppableDateBox } from '../components/dnd/DroppableDateBox';
import { getGroupedAppointments, GroupedAppointments } from '../utils/DtoGroupingUtils';
import { AppointmentUpdateDto, AppointmentReadDto } from '../types/ApiTypes';
import { AppointmentList } from '../components/appointments/AppointmentList';
import { RouterProps } from 'react-router';

interface Props extends RouterProps {
    appointments: AppointmentReadDto[];
    onUpdate: (id: number, updated: AppointmentUpdateDto) => any;
    isStatusChangeEnabled: boolean;
}
export const Appointments = (props: Props) => {
    const { onUpdate, isStatusChangeEnabled, history } = props;
    const [appointmentGroups, setAppointmentGroups] = useState(getGroupedAppointments(props.appointments));

    const handleDrop = (result: DropResult) => {
        const destination = result.destination;
        const source = result.source;
        if (!destination) return;
        if (destination.droppableId === source.droppableId && destination.index === source.index) return;

        const sourceGroup = appointmentGroups[source.droppableId as keyof GroupedAppointments];
        const destinationGroup = appointmentGroups[destination.droppableId as keyof GroupedAppointments];

        const appointment = sourceGroup[source.index];
        const updatedAppointment = {
            ...appointment,
            status: destination.droppableId.toUpperCase() as "OPEN" | "RESERVED" | "FIXED" | "DISMISSED",
        };

        sourceGroup.splice(source.index, 1);
        destinationGroup.splice(destination.index, 0, updatedAppointment);

        const updatedGroups = {
            ...appointmentGroups,
            [source.droppableId]: sourceGroup,
            [destination.droppableId]: destinationGroup,
        }
        onUpdate(updatedAppointment.id, { status: updatedAppointment.status, start: updatedAppointment.start, end: updatedAppointment.end, name: updatedAppointment.name });
        setAppointmentGroups(updatedGroups);
    }

    const handleAppointmentContextClick = (appointment: AppointmentReadDto) => {
        history.push(`/appointments/${appointment.id}`);
    }

    const handleAppointmentClick = (appointment: AppointmentReadDto) => {
        history.push(`/appointments/${appointment.id}/votes`);
    }

    const commonProps = {
        isStatusChangeEnabled,
        onContextClick: handleAppointmentContextClick,
        onClick: handleAppointmentClick,
    }

    return (
        <DragDropContext onDragEnd={handleDrop}>
            <Grid columns={4} doubling stackable>
                <Grid.Column >
                    <DroppableDateBox name='Verworfene Termine' id='dismissed' color='red' >
                        <AppointmentList
                            color='red'
                            appointments={appointmentGroups.dismissed}
                            {...commonProps}
                        />
                    </DroppableDateBox>
                </Grid.Column>
                <Grid.Column>
                    <DroppableDateBox name='Offene Termine' id='open' color='blue' >
                        <AppointmentList
                            color='blue'
                            appointments={appointmentGroups.open}
                            {...commonProps}
                        />
                    </DroppableDateBox>
                </Grid.Column>
                <Grid.Column>
                    <DroppableDateBox name='Reservierte Termine' id='reserved' color='teal' >
                        <AppointmentList
                            color='teal'
                            appointments={appointmentGroups.reserved}
                            {...commonProps}
                        />
                    </DroppableDateBox>
                </Grid.Column>
                <Grid.Column>
                    <DroppableDateBox name='Fixierte Termine' id='fixed' color='green' >
                        <AppointmentList
                            color='green'
                            appointments={appointmentGroups.fixed}
                            {...commonProps}
                        />
                    </DroppableDateBox>
                </Grid.Column>
            </Grid>
        </DragDropContext>
    )
}
