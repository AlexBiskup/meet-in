import React, { useEffect } from 'react';
import { TransitionContainer } from '../components/hoc/TrainsitionContainer';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import { RouterProps } from 'react-router';
import { ProjectBoard } from '../components/projects/ProjectBoard';
import { NewProject } from '../components/projects/NewProject';
import { useRest } from '../hooks/UseRest';
import { projectsResource } from '../rest/Resources';
import { notifySuccess } from '../utils/Notifications';
import { Rest } from '../components/hoc/Rest';
import { SingleProject } from './SingleProject';

interface Props extends RouterProps {

}
export const Projects = (props: Props) => {
    const { history } = props;

    const getProjects = useRest(projectsResource.getAll);
    const projects = (
        <Rest
            {...getProjects}
            executeOnMount={get => get()}
            render={(projects) => <ProjectBoard projects={projects} />}
        />
    )

    const afterCreateProject = () => {
        history.goBack();
        notifySuccess('Das Projekt wurde erstellt');
    }

    const createProject = useRest(projectsResource.create, afterCreateProject);

    const newProjectProps = {
        onClose: history.goBack,
        onCreateProject: createProject.execute,
        isCreationPending: createProject.isPending,
    }

    return (
        <TransitionContainer>
            <BrowserRouter basename='/projects'>
                <Switch>
                    <Route path='/' exact={true} render={() => projects} />
                    <Route path='/new' exact={true} render={() => <NewProject {...newProjectProps} />} />
                    <Route path='/:projectId' render={(props) =>
                        <SingleProject key={`project:${props.match.params.projectId}`} projectId={props.match.params.projectId} {...props} />}
                    />
                </Switch>
            </BrowserRouter>
        </TransitionContainer>
    )
}


