import { AxiosRestClient } from '@abiskup/ts-rest-client';
import axios from 'axios';
import { AUTH_TOKEN_KEY } from '../containers/App';

export const BASE_URL = 'http://localhost:8080';
export const TOKEN_HEADER_NAME = 'Authorization';

const appAxios = axios.create({
    baseURL: BASE_URL,
});

appAxios.interceptors.request.use(
    (request) => {
        const authToken = localStorage.getItem(AUTH_TOKEN_KEY);
        if (authToken) {
            request.headers[TOKEN_HEADER_NAME] = authToken;
        }
        return request;
    }
)

export const appClient = new AxiosRestClient(BASE_URL, appAxios);
export const preAppClient = new AxiosRestClient(BASE_URL);
