import { ProjectsResource } from './resources/ProjectsResource';
import { AuthResource } from './resources/AuthResource';
import { appClient, preAppClient } from './Client';
import { SignupResource } from './resources/SignupResource';
import { AppointmentsResource } from './resources/AppointmentsResource';
import { ParticipantsResource } from './resources/ParticipantsResource';
import { VotesResource } from './resources/VotesResource';

export const authResource = new AuthResource(preAppClient);

export const signupResource = new SignupResource(preAppClient);

export const projectsResource = new ProjectsResource(appClient);

export const appointmentsResource = new AppointmentsResource(appClient);

export const participantsResource = new ParticipantsResource(appClient);

export const votesResource = new VotesResource(appClient);