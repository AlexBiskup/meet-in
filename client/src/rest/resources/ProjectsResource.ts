import { RestResource } from "@abiskup/ts-rest-client";
import { ProjectCreateDto, ProjectReadDto, ProjectUpdateDto } from '../../types/ApiTypes';

export class ProjectsResource extends RestResource {

    protected basePath = '/api/projects';

    getAll = (): Promise<ProjectReadDto[]> => {
        return this.get();
    }

    getById = (projectId: number): Promise<ProjectReadDto> => {
        return this.get({
            url: `/${projectId}`
        });
    }

    create = (project: ProjectCreateDto): Promise<number> => {
        return this.post({
            data: project
        });
    }

    update = (projectId: number, project: ProjectUpdateDto): Promise<void> => {
        return this.put({
            url: `/${projectId}`,
            data: project
        });
    }

}