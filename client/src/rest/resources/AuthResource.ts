import { RestResource } from "@abiskup/ts-rest-client";
import { AuthenticationRequest, AuthenticationResponse, RefreshTokenRequest } from "../../types/AuthTypes";

export class AuthResource extends RestResource {

    protected basePath = '/auth';

    authenticate = (credentials: AuthenticationRequest): Promise<AuthenticationResponse> => {
        return this.post({
            data: credentials,
        })
    }

    refresh = (refreshData: RefreshTokenRequest): Promise<AuthenticationResponse> => {
        return this.post({
            url: '/refresh',
            data: refreshData,
        })
    }

}