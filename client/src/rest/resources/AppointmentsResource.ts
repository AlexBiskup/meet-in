import { RestResource } from "@abiskup/ts-rest-client";
import { AppointmentCreateDto, AppointmentReadDto, AppointmentUpdateDto, AppointmentDetailsDto } from "../../types/ApiTypes";


export class AppointmentsResource extends RestResource {

    protected basePath = '/api/appointments';

    getById = (appointmentId: number): Promise<AppointmentDetailsDto> => {
        return this.get({
            url: `/${appointmentId}`
        });
    }

    deleteById = (appointmentId: number): Promise<void> => {
        return this.delete({
            url: `/${appointmentId}`
        });
    }

    create = (appointment: AppointmentCreateDto): Promise<number> => {
        return this.post({
            data: appointment
        });
    }

    getAll = (projectId: number): Promise<AppointmentReadDto[]> => {
        return this.get({
            url: `?projectId=${projectId}`
        })
    }

    update = (id: number, appoinment: AppointmentUpdateDto): Promise<void> => {
        return this.put({
            url: `/${id}`,
            data: appoinment,
        });
    }

}