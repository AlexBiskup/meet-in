import { RestResource } from "@abiskup/ts-rest-client";
import { ParticipantCreateDto, ParticipantListDto } from "../../types/ApiTypes";

export class ParticipantsResource extends RestResource {

    protected basePath = '/api/participants';

    create = (participant: ParticipantCreateDto, projectId: number): Promise<number> => {
        return this.post({
            url: `?projectId=${projectId}`,
            data: participant,
        })
    }

    getAll = (projectId: number): Promise<ParticipantListDto[]> => {
        return this.get({
            url: `?projectId=${projectId}`,
        })
    }

    deleteParticipant = (participantId: number): Promise<void> => {
        return this.delete({
            url: `/${participantId}`
        })
    }

}