import { RestResource } from "@abiskup/ts-rest-client";
import { UserCreateDto } from "../../types/ApiTypes";


export class SignupResource extends RestResource {

    protected basePath = '/api/signup';

    signup = (user: UserCreateDto): Promise<number> => {
        return this.post({
            data: user
        })
    }
}