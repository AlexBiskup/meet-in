import { RestResource } from "@abiskup/ts-rest-client";
import { VoteReadDto, VoteUpdateDto } from "../../types/ApiTypes";


export class VotesResource extends RestResource {

    protected basePath = '/api/votes';

    getAll = (projectId: number): Promise<VoteReadDto[]> => {
        return this.get({
            url: `?projectId=${projectId}`
        });
    }

    update = (voteId: number, vote: VoteUpdateDto): Promise<void> => {
        return this.put({
            url: `/${voteId}`,
            data: vote,
        });
    }

}