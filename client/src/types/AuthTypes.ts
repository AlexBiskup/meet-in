/**
 * AuthenticationRequest
 */
export interface AuthenticationRequest {
    username: string;
    password: string;
}
/**
 * AuthenticationResponse
 */
export interface AuthenticationResponse {
    expirationDate: string; // date-time
    refreshToken: string;
    token: string;
}
/**
 * RefreshTokenRequest
 */
export interface RefreshTokenRequest {
    token: string;
    refreshToken: string;
}
