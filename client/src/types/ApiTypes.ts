/**
 * AppointmentCreateDto
 */
export interface AppointmentCreateDto {
    projectId?: number; // int64
    start: string; // date-time
    end: string; // date-time
    name?: string;
}
/**
 * AppointmentDetailsDto
 */
export interface AppointmentDetailsDto {
    end: string; // date-time
    id: number; // int64
    name?: string;
    start: string; // date-time
    status: "OPEN" | "RESERVED" | "FIXED" | "DISMISSED";
    voteStats: VoteStatsDto;
    votes: VoteEmbeddedDto[];
}
/**
 * AppointmentReadDto
 */
export interface AppointmentReadDto {
    end: string; // date-time
    id: number; // int64
    name?: string;
    start: string; // date-time
    status: "OPEN" | "RESERVED" | "FIXED" | "DISMISSED";
    voteStats: VoteStatsDto;
}
/**
 * AppointmentUpdateDto
 */
export interface AppointmentUpdateDto {
    status: "OPEN" | "RESERVED" | "FIXED" | "DISMISSED";
    start: string; // date-time
    end: string; // date-time
    name?: string;
}
/**
 * ParticipantCreateDto
 */
export interface ParticipantCreateDto {
    email: string;
}
/**
 * ParticipantListDto
 */
export interface ParticipantListDto {
    id: number; // int64
    user: UserReadDto;
}
/**
 * ProjectCreateDto
 */
export interface ProjectCreateDto {
    name: string;
    autoDismiss?: boolean;
    autoFixing?: boolean;
}
/**
 * ProjectReadDto
 */
export interface ProjectReadDto {
    autoDismiss: boolean;
    autoFixing: boolean;
    id: number; // int64
    name: string;
    owner: UserReadDto;
    participantIsOwner: boolean;
}
/**
 * ProjectUpdateDto
 */
export interface ProjectUpdateDto {
    name: string;
    autoDismiss?: boolean;
    autoFixing?: boolean;
}
/**
 * UserCreateDto
 */
export interface UserCreateDto {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}
/**
 * UserReadDto
 */
export interface UserReadDto {
    email: string;
    firstName: string;
    lastName: string;
}
/**
 * VoteEmbeddedDto
 */
export interface VoteEmbeddedDto {
    id: number; // int64
    participant: ParticipantListDto;
    status: "UNANSWERED" | "APPROVED" | "DECLINED";
}
/**
 * VoteReadDto
 */
export interface VoteReadDto {
    appointment: AppointmentReadDto;
    id: number; // int64
    participantId: number; // int64
    status: "UNANSWERED" | "APPROVED" | "DECLINED";
}
/**
 * VoteStatsDto
 */
export interface VoteStatsDto {
    approvedCount: number; // int64
    declinedCount: number; // int64
    voteCount: number; // int64
}
/**
 * VoteUpdateDto
 */
export interface VoteUpdateDto {
    status: "UNANSWERED" | "APPROVED" | "DECLINED";
}
