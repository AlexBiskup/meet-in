import React from 'react';
import { Modal, Container, Divider } from "semantic-ui-react";
import { appointmentsResource } from "../../rest/Resources";
import { useRest } from "../../hooks/UseRest";
import { Rest } from "../hoc/Rest";
import { getFormattedStartAndEnd } from '../../utils/DateUtils';
import { AppointmentVotesList } from './AppointmentVotesList';
import { VoteStats } from './VoteStats';

interface Props {
    onClose: () => void;
    appointmentId: number;
}

export function AppointmentVotes(props: Props) {
    const { onClose, appointmentId } = props;
    const getAppointment = useRest(appointmentsResource.getById);

    return (
        <Modal open={true} onClose={onClose} centered={false} dimmer='inverted'>

            <Rest {...getAppointment} executeOnMount={get => get(appointmentId)}>
                {(appointment) => {
                    const formattedDates = getFormattedStartAndEnd(appointment.start, appointment.end);
                    const header = appointment.name || `${formattedDates.start} - ${formattedDates.end}`;
                    return (
                        <React.Fragment>
                            <Modal.Header>{header} </Modal.Header>
                            <Modal.Content>
                                <Container text>
                                    <VoteStats voteStats={appointment.voteStats} size='big' />
                                    <Divider hidden />
                                    <AppointmentVotesList votes={appointment.votes} />
                                </Container>
                            </Modal.Content>
                        </React.Fragment>
                    )
                }}
            </Rest>
        </Modal>
    )
}