import React from 'react';
import { AppointmentCreateDto } from '../../types/ApiTypes';
import { TransitionContainer } from '../hoc/TrainsitionContainer';
import { Modal, Container } from 'semantic-ui-react';
import { AppointmentForm, AppointmentFormData } from './AppointmentForm';

interface Props {
    onCreate: (appointment: AppointmentCreateDto) => any;
    onClose: () => any;
    isCreationPending: boolean;
    projectId: number;
}
export const NewAppointment = (props: Props) => {
    const { onCreate, onClose, isCreationPending, projectId } = props;
    const handleSubmit = (formData: AppointmentFormData) => {
        onCreate({
            projectId,
            ...formData
        })
    }
    const currentDateString = new Date().toJSON();
    return (
        <TransitionContainer>
            <Modal open={true} onClose={onClose} centered={false} dimmer='inverted'>
                <Modal.Header>Neuer Termin</Modal.Header>
                <Modal.Content>
                    <Container text>
                        <AppointmentForm
                            onSubmit={handleSubmit}
                            isPending={isCreationPending}
                            appointment={{ name: '', start: currentDateString, end: currentDateString }}
                        />
                    </Container>
                </Modal.Content>
            </Modal>
        </TransitionContainer>
    )
}