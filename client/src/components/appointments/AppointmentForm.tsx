import React, { useState } from 'react';
import { Form, Button, Divider } from 'semantic-ui-react';
import Datepicker from 'react-datepicker';
import '../../css/styles.css';
import { Input } from '../forms/Input';

export interface AppointmentFormData {
    start: string;
    end: string;
    name: string;
}
interface Props {
    onSubmit: (appointment: AppointmentFormData) => any;
    isPending: boolean;
    appointment: AppointmentFormData;
}
export const AppointmentForm = (props: Props) => {
    const { isPending, onSubmit, appointment } = props;
    const now = new Date();
    const [start, setStart] = useState<Date | null>(new Date(appointment.start));
    const [end, setEnd] = useState<Date | null>(new Date(appointment.end));
    const [name, setName] = useState(appointment.name);

    const handleSetStart = (date: Date) => {
        setStart(date);
        setEnd(date);
    }

    const handleSubmit = () => {
        if (start === null || end === null) return;
        onSubmit({
            start: start.toJSON(),
            end: end.toJSON(),
            name
        })
    }
    return (
        <Form onSubmit={handleSubmit} loading={isPending}>
            <Form.Field required>
                <label>Beginn</label>
                <Datepicker
                    selected={start}
                    onChange={handleSetStart}
                    dateFormat="dd.MM.yyyy HH:mm"
                    showTimeSelect
                    timeFormat="HH:mm"
                    minDate={now}
                    className={"ui fluid"}
                    monthsShown={2}
                />
            </Form.Field>
            <Form.Field required>
                <label>Ende</label>
                <Datepicker
                    selected={end}
                    onChange={setEnd}
                    dateFormat="dd.MM.yyyy HH:mm"
                    showTimeSelect
                    timeFormat="HH:mm"
                    minDate={start}
                    className="fullWidth"
                    monthsShown={2}
                />
            </Form.Field>
            <Form.Field>
                <label>Beschreibung</label>
                <Input value={name} onChange={setName} placeholder='Beschreibung' />
            </Form.Field>
            <Divider hidden />
            <Button disabled={false} color={'green'} type='submit'>Speichern</Button>
        </Form>
    )
}