import React from 'react';
import { VoteEmbeddedDto } from '../../types/ApiTypes';
import { Table, Icon } from 'semantic-ui-react';

interface Props {
    votes: VoteEmbeddedDto[];
}
export const AppointmentVotesList = (props: Props) => {
    const { votes } = props;

    return (
        <Table basic='very' size='small'>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nachname</Table.HeaderCell>
                    <Table.HeaderCell>Vorname</Table.HeaderCell>
                    <Table.HeaderCell>Email</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center'>Status</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {votes.map(
                    (vote, index) => <Row key={index} vote={vote} />
                )}
            </Table.Body>
        </Table>
    )
}

interface RowProps {
    vote: VoteEmbeddedDto;
}
const Row = (props: RowProps) => {
    const { vote } = props;
    const user = vote.participant.user;
    const iconProps = getIconProps(vote);
    return (
        <Table.Row>
            <Table.Cell>
                {user.lastName}
            </Table.Cell>
            <Table.Cell>
                {user.firstName}
            </Table.Cell>
            <Table.Cell>
                {user.email}
            </Table.Cell>
            <Table.Cell textAlign='center'>
                <Icon name={iconProps.name} color={iconProps.color} />
            </Table.Cell>
        </Table.Row>
    )
}

function getIconProps(vote: VoteEmbeddedDto): IconProps {
    const status = vote.status;
    switch (status) {
        case 'UNANSWERED': return { name: 'question', color: 'blue' };
        case 'APPROVED': return { name: 'check', color: 'green' };
        case 'DECLINED': return { name: 'x', color: 'red' };
    }
}

type IconProps = {
    name: 'question' | 'check' | 'x';
    color: 'blue' | 'green' | 'red';
}