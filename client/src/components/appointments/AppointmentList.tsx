import React from 'react';
import { AppointmentReadDto } from '../../types/ApiTypes';
import { Appointment } from './Appointment';
import { Color } from '../../types/Colors';

interface Props {
    appointments: AppointmentReadDto[];
    color: Color;
    isStatusChangeEnabled: boolean;
    onContextClick: (appointment: AppointmentReadDto) => any;
    onClick: (appointment: AppointmentReadDto) => any;
};
export const AppointmentList = (props: Props) => {
    const { appointments, color, isStatusChangeEnabled, onContextClick, onClick } = props;
    return (
        <React.Fragment>
            {appointments.map(
                (appointment, index) => (
                    <Appointment
                        key={index}
                        index={index}
                        appointment={appointment}
                        color={color}
                        isDragEnabled={isStatusChangeEnabled}
                        onContextClick={onContextClick}
                        onClick={onClick}
                    />
                )
            )}
        </React.Fragment>
    )
}