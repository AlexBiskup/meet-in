import React from 'react';
import { Modal, Container, Popup, Button, Divider } from 'semantic-ui-react';
import { useRest } from '../../hooks/UseRest';
import { appointmentsResource } from '../../rest/Resources';
import { Rest } from '../hoc/Rest';
import { AppointmentForm, AppointmentFormData } from './AppointmentForm';
import { notifySuccess } from '../../utils/Notifications';

interface Props {
    onClose: () => any;
    appointmentId: number;
}
export const AppointmentDetails = (props: Props) => {
    const { onClose, appointmentId } = props;

    const afterUpdate = () => {
        onClose();
        notifySuccess('Die Änderungen wurden gespeichert')
    }

    const afterDelete = () => {
        onClose();
        notifySuccess('Der Termin wurde gelöscht');
    }

    const getAppointment = useRest(appointmentsResource.getById)
    const updateAppointment = useRest(appointmentsResource.update, afterUpdate);
    const deleteAppointment = useRest(appointmentsResource.deleteById, afterDelete)

    return (
        <Modal open={true} onClose={onClose} centered={false} dimmer='inverted'>
            <Modal.Header>Termin bearbeiten</Modal.Header>
            <Rest {...getAppointment} executeOnMount={get => get(appointmentId)}>
                {(appointment) => {
                    const handleSubmit = (formData: AppointmentFormData) => {
                        updateAppointment.execute(appointmentId, { status: appointment.status, ...formData });
                    }
                    return (
                        <Modal.Content>
                            <Container text>
                                <AppointmentForm
                                    onSubmit={handleSubmit}
                                    isPending={updateAppointment.isPending || deleteAppointment.isPending}
                                    appointment={{ name: appointment.name || '', start: appointment.start, end: appointment.end }}
                                />
                                <Divider />
                                <Popup
                                    trigger={<Button negative content='Löschen' />}
                                    content={<Button icon='exclamation' content='Unwiderruflich löschen' onClick={() => deleteAppointment.execute(appointmentId)} />}
                                    on='click'
                                    position='top center'
                                />
                            </Container>
                        </Modal.Content>
                    )
                }}
            </Rest>
        </Modal>
    )
}