import React from 'react';
import { AppointmentReadDto } from '../../types/ApiTypes';
import { Color } from '../../types/Colors';
import { DraggableAppointmentCard } from '../dnd/DraggableAppointmentCard';

interface Props {
    color: Color;
    appointment: AppointmentReadDto;
    index: number;
    isDragEnabled: boolean;
    onContextClick: (appointment: AppointmentReadDto) => any;
    onClick: (appointment: AppointmentReadDto) => any;
}
export const Appointment = (props: Props) => {
    const { color, appointment, index, isDragEnabled, onContextClick, onClick } = props;

    return (
        <DraggableAppointmentCard
            id={appointment.id.toString()}
            index={index} color={color}
            appointment={appointment}
            displayStats={true}
            isDragEnabled={isDragEnabled}
            onContextClick={onContextClick}
            onClick={onClick}
        />
    )
}