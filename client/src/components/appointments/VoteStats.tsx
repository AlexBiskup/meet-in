import React from 'react';
import { VoteStatsDto } from '../../types/ApiTypes';

interface Props {
    voteStats: VoteStatsDto;
    size?: 'small' | 'big';
}
export const VoteStats = (props: Props) => {
    const { voteStats: { declinedCount, approvedCount, voteCount }, size = 'small' } = props;

    const approvedPercentage = Math.round(calculatePercentage(voteCount, approvedCount));
    const declinedPercentage = Math.round(calculatePercentage(voteCount, declinedCount));

    const approvedBorderRadius = approvedPercentage === 100 ? '2px' : '2px 0px 0px 2px';
    const declinedBorderRadius = approvedPercentage === 0 ? '2px' : '0px 2px 2px 0px';

    const height = size === 'small' ? 15 : 25;
    const fontSize = size === 'small' ? '10px' : '15px';

    return (
        <div style={{ height: height, width: '100%', background: 'rgba(0,0,0,.25)', borderRadius: 2, textAlign: 'center', lineHeight: `${height}px`, color: 'white', fontSize }}>
            <div
                style={{ height: '100%', width: `${approvedPercentage}%`, background: '#21ba45', float: 'left', borderRadius: approvedBorderRadius, display: 'border-box' }}>
                {approvedCount > 0 && approvedPercentage + '%'}
            </div>
            <div
                style={{ height: '100%', width: `${declinedPercentage}%`, background: '#db2828', float: 'left', borderRadius: declinedBorderRadius }}>
                {declinedCount > 0 && declinedPercentage + '%'}
            </div>
        </div>
    )
}


function calculatePercentage(whole: number, part: number): number {
    if (whole === 0) return 0;
    return (part / whole) * 100;
}
