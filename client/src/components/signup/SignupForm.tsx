import React, { useState } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { ValidatedInput, isNotEmpty, isValidEmail, isMinLength, valueEquals } from '../forms/ValidatedInput';
import { UserCreateDto } from '../../types/ApiTypes';

interface Props {
    isPending: boolean;
    onSubmit: (user: UserCreateDto) => void;
}
export const SignupForm = (props: Props) => {

    const { isPending, onSubmit } = props;

    const [firstNameState, setFirstNameState] = useState({ value: '', isValid: false });
    const [lastNameState, setLastNameState] = useState({ value: '', isValid: false });
    const [emailState, setEmailState] = useState({ value: '', isValid: false });
    const [passwordState, setPasswordState] = useState({ value: '', isValid: false });
    const [passwordRepeatState, setPasswordRepeatState] = useState({ value: '', isValid: false });

    const isFormValid = (
        firstNameState.isValid &&
        lastNameState.isValid &&
        emailState.isValid &&
        passwordState.isValid &&
        passwordRepeatState.isValid
    );

    const handleSubmit = () => {
        if (!isFormValid) return;
        onSubmit({
            firstName: firstNameState.value,
            lastName: lastNameState.value,
            email: emailState.value,
            password: passwordState.value,
        });
    }

    return (
        <Form loading={isPending} onSubmit={handleSubmit}>
            <Form.Field required error={!firstNameState.isValid}>
                <label>Vorname</label>
                <ValidatedInput
                    inputState={firstNameState}
                    onChange={setFirstNameState}
                    rules={[isNotEmpty]}
                    placeholder='Vorname'
                />
            </Form.Field>
            <Form.Field required error={!lastNameState.isValid}>
                <label>Nachname</label>
                <ValidatedInput
                    inputState={lastNameState}
                    onChange={setLastNameState}
                    rules={[isNotEmpty]}
                    placeholder='Nachname'
                />
            </Form.Field>
            <Form.Field required error={!emailState.isValid}>
                <label>Email</label>
                <ValidatedInput
                    inputState={emailState}
                    onChange={setEmailState}
                    rules={[isValidEmail]}
                    placeholder='Email'
                />
            </Form.Field>
            <Form.Field required error={!passwordState.isValid}>
                <label>Passwort</label>
                <ValidatedInput
                    type='password'
                    inputState={passwordState}
                    onChange={setPasswordState}
                    rules={[(value) => isMinLength(value, 5)]}
                    placeholder='Passwort'
                />
            </Form.Field>
            <Form.Field required error={!passwordRepeatState.isValid}>
                <label>Passwort wiederholen</label>
                <ValidatedInput
                    type='password'
                    inputState={passwordRepeatState}
                    onChange={setPasswordRepeatState}
                    rules={[
                        (value) => valueEquals(value, passwordState.value),
                        (value) => isMinLength(value, 5)
                    ]}
                    revalidateOn={[passwordState.value]}
                    placeholder='Passwort wiederholen'
                />
            </Form.Field>
            <Button color={'green'} type='submit' disabled={!isFormValid}>Registrieren</Button>
        </Form>
    )
}

