import React, { useState } from 'react';
import { Form, Button, Checkbox, Icon, Divider, Popup } from 'semantic-ui-react';
import { ValidatedInput, isMinLength } from '../forms/ValidatedInput';
import { ProjectCreateDto } from '../../types/ApiTypes';

interface Props {
    onSubmit: (project: ProjectCreateDto) => any;
    isPending: boolean;
    project: ProjectCreateDto;
}
export const ProjectForm = (props: Props) => {
    const { onSubmit, isPending, project } = props;
    const [nameState, setNameState] = useState({ value: project.name, isValid: false })
    const [autoReserve, setAutoReserve] = useState(project.autoFixing);
    const [autoDismiss, setAutoDismiss] = useState(project.autoDismiss);
    const isFormValid = nameState.isValid
    const handleSubmit = () => {
        onSubmit({
            ...project,
            name: nameState.value,
            autoFixing: autoReserve,
            autoDismiss: autoDismiss
        });
    }
    return (
        <Form onSubmit={handleSubmit} loading={isPending}>
            <Form.Field required error={!nameState.isValid}>
                <label>Name</label>
                <ValidatedInput
                    inputState={nameState}
                    onChange={setNameState}
                    placeholder='Name'
                    rules={[(value) => isMinLength(value, 3)]}
                />
            </Form.Field>
            <Form.Group widths='2'>
                <Form.Field>
                    <Checkbox label='Termine automatisch reservieren'
                        checked={autoReserve === true}
                        onChange={() => setAutoReserve(!autoReserve)}
                    />
                    <Popup
                        trigger={<Icon name='question' color='blue' />}
                        content='Sobald alle Teilnehmer dem Termin zugesagt haben, wird der Termin automatisch in die reservierten Termine eingetragen.'
                    />
                </Form.Field>
                <Form.Field>
                    <Checkbox
                        label='Termine automatisch verwerfen'
                        onChange={() => setAutoDismiss(!autoDismiss)}
                        checked={autoDismiss}
                    />
                    <Popup
                        trigger={<Icon name='question' color='blue' />}
                        content='Sobald ein Teilnehmer den Termin abgesagt hat, wird der Termin automatisch in die verworfenen Termine eingetragen.'
                    />
                </Form.Field>
            </Form.Group>
            <Divider hidden />
            <Button disabled={!isFormValid} color={'green'} type='submit'>Speichern</Button>
        </Form>
    )
}