import React from 'react';
import { ProjectReadDto } from '../../types/ApiTypes';
import { Header, Divider, Card, Button, List } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

interface Props {
    projects: ProjectReadDto[],
    title: string,
    displayNewButton?: boolean,
    displayOwner?: boolean,
}
export const ProjectList = (props: Props) => {
    const { title, projects, displayNewButton, displayOwner } = props;
    return (
        <React.Fragment>
            <Header as='h2'>
                {title}
                {displayNewButton && (
                    <Button as={Link} to='/new' color='green' content='Neues Projekt' floated='right' />
                )}
            </Header>
            <Divider hidden />
            <List divided relaxed>
                {projects.map(
                    (project, index) => <ProjectItem key={index} project={project} displayOwner={displayOwner || false} />
                )}
            </List>
        </React.Fragment >
    )
}

interface ItemProps {
    project: ProjectReadDto;
    displayOwner: boolean;
}
export const ProjectItem = (props: ItemProps) => {
    const { project, displayOwner } = props;
    return (
        <List.Item>
            <List.Icon name='calendar alternate outline' size='large' verticalAlign='middle' />
            <List.Content verticalAlign='middle'>
                <List.Header as={Link} to={`/${project.id}`}>
                    {project.name}
                </List.Header>
                {displayOwner && (
                    <List.Description>
                        <small> {`${project.owner.firstName} ${project.owner.lastName}`} </small>
                    </List.Description>
                )}
            </List.Content>
        </List.Item>
    )
}