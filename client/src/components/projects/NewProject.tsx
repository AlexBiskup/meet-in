import React from 'react';
import { Modal, Container } from 'semantic-ui-react';
import { ProjectForm } from './ProjectForm';
import { TransitionContainer } from '../hoc/TrainsitionContainer';
import { ProjectCreateDto } from '../../types/ApiTypes';

interface Props {
    onClose: () => any;
    onCreateProject: (newProject: ProjectCreateDto) => any;
    isCreationPending: boolean;
}
export const NewProject = (props: Props) => {
    const { onClose, onCreateProject, isCreationPending } = props;
    return (
        <TransitionContainer>
            <Modal open={true} onClose={onClose} centered={false} dimmer='inverted'>
                <Modal.Header>Neues Projekt</Modal.Header>
                <Modal.Content>
                    <Container text>
                        <ProjectForm
                            onSubmit={onCreateProject}
                            isPending={isCreationPending}
                            project={{ name: '', autoDismiss: false, autoFixing: false }}
                        />
                    </Container>
                </Modal.Content>
            </Modal>
        </TransitionContainer>
    )
}