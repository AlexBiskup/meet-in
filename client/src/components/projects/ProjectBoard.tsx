import React from 'react';
import { ProjectReadDto } from '../../types/ApiTypes';
import { ProjectList } from './ProjectList';
import { Divider } from 'semantic-ui-react';

interface Props {
    projects: ProjectReadDto[]
}
export const ProjectBoard = (props: Props) => {
    const { projects } = props;

    const myProjects: ProjectReadDto[] = [];
    const otherProjects: ProjectReadDto[] = [];

    projects.forEach(
        project => {
            if (project.participantIsOwner) {
                myProjects.push(project);
            } else {
                otherProjects.push(project);
            }
        }
    )

    return (
        <React.Fragment>
            <Divider hidden />
            <ProjectList projects={myProjects} title='Meine Projekte' displayNewButton={true} />
            <Divider hidden />
            <Divider hidden />
            <ProjectList projects={otherProjects} title='Teilnahmen' displayOwner={true} />
        </React.Fragment>
    )
}
