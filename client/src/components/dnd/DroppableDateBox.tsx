import React from 'react';
import { Segment } from 'semantic-ui-react';
import { Droppable } from 'react-beautiful-dnd';
import { Color } from '../../types/Colors';

interface Props {
    color: Color;
    children: JSX.Element;
    id: string;
    name: string;
}
export const DroppableDateBox = (props: Props) => {
    const { color, id, name, children } = props;
    return (
        <Droppable droppableId={id} >
            {(provided) => (
                <Segment color={color} tertiary inverted textAlign='center'>
                    <strong>{name}</strong>
                    <div {...provided.droppableProps} ref={provided.innerRef} style={{ minHeight: '5rem' }}>
                        {children}
                        {provided.placeholder}
                    </div>
                </Segment>
            )}
        </Droppable>
    )
}
