import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { AppointmentReadDto } from '../../types/ApiTypes';
import { Grid, Icon } from 'semantic-ui-react';
import { Color } from '../../types/Colors';
import { getFormattedStartAndEnd } from '../../utils/DateUtils';
import { VoteStats } from '../appointments/VoteStats';

interface Props {
    id: string;
    index: number;
    appointment: AppointmentReadDto;
    color: Color;
    displayStats: boolean;
    isDragEnabled: boolean;
    onContextClick: (appointment: AppointmentReadDto) => any;
    onClick: (appointment: AppointmentReadDto) => any;
}
export const DraggableAppointmentCard = (props: Props) => {
    const { id, index, appointment, color, displayStats, isDragEnabled, onContextClick, onClick } = props;
    const formattedDates = getFormattedStartAndEnd(appointment.start, appointment.end);

    const handleContextClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        event.preventDefault();
        onContextClick(appointment);
    }

    const handleClick = () => {
        onClick(appointment);
    }

    return (
        <Draggable draggableId={id} index={index} isDragDisabled={!isDragEnabled}>
            {(provided) => (
                <div
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}
                    className="ui raised segment"
                    style={{ ...provided.draggableProps.style, margin: 0, marginTop: '1rem', cursor: isDragEnabled ? 'grab' : 'pointer' }}
                    onContextMenu={handleContextClick}
                    onClick={handleClick}
                >
                    <Grid columns='2'>
                        <Grid.Column width='7' textAlign='left'>
                            {!appointment.name && (
                                <Icon name='calendar' color={color} size='large' />
                            )}
                            {appointment.name && (
                                <strong>{appointment.name}</strong>
                            )}
                            <div style={{ width: '100%', height: 10 }} />
                            {displayStats && (
                                <VoteStats voteStats={appointment.voteStats} />
                            )}
                        </Grid.Column>
                        <Grid.Column textAlign='right' width='9'>
                            <div>{formattedDates.start}</div>
                            <div>{formattedDates.end}</div>
                        </Grid.Column>
                    </Grid>
                </div>
            )
            }
        </Draggable >
    )
}