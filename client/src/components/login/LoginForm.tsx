import React, { useState } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { AuthenticationRequest } from '../../types/AuthTypes';
import { ValidatedInput, isNotEmpty } from '../forms/ValidatedInput';

interface Props {
    onSubmit: (credentials: AuthenticationRequest) => void;
    isPending: boolean;
}
export const LoginForm = (props: Props) => {
    const { onSubmit, isPending } = props;
    const [emailState, setEmailState] = useState({ value: '', isValid: false });
    const [passwordState, setPasswordState] = useState({ value: '', isValid: false });

    const isFormValid = emailState.isValid && passwordState.isValid;

    const handleSubmit = () => {
        if (!isFormValid) return;
        onSubmit({
            username: emailState.value,
            password: passwordState.value,
        });
    }

    return (
        <Form loading={isPending} onSubmit={handleSubmit}>
            <Form.Field required>
                <label>Email</label>
                <ValidatedInput
                    inputState={emailState}
                    onChange={setEmailState}
                    placeholder='Email'
                    rules={[isNotEmpty]}
                />
            </Form.Field>
            <Form.Field required>
                <label>Passwort</label>
                <ValidatedInput
                    inputState={passwordState}
                    onChange={setPasswordState}
                    placeholder='Passwort'
                    type='password'
                    rules={[isNotEmpty]}
                />
            </Form.Field>
            <Button disabled={!isFormValid} color={'green'} type='submit'>Login</Button>
        </Form>
    )
}