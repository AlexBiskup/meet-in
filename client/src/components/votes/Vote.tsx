import React from 'react';
import { DraggableAppointmentCard } from '../dnd/DraggableAppointmentCard';
import { VoteReadDto } from '../../types/ApiTypes';
import { Color } from '../../types/Colors';

interface Props {
    vote: VoteReadDto;
    color: Color;
    index: number;
}
export const Vote = (props: Props) => {
    const { vote, color, index } = props;
    return (
        <DraggableAppointmentCard
            id={vote.id.toString()}
            index={index} color={color}
            appointment={vote.appointment}
            displayStats={false} isDragEnabled={true}
            onContextClick={() => null}
            onClick={() => null}
        />
    )
}