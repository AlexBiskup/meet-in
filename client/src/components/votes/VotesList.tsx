import React from 'react';
import { VoteReadDto } from '../../types/ApiTypes';
import { Color } from '../../types/Colors';
import { Vote } from './Vote';

interface Props {
    votes: VoteReadDto[];
    color: Color;
}
export const VotesList = (props: Props) => {
    const { votes, color } = props;
    return (
        <React.Fragment>
            {votes.map(
                (vote, index) => <Vote key={index} index={index} vote={vote} color={color} />
            )}
        </React.Fragment>
    )
}