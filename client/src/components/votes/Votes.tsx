import React, { useState } from 'react';
import { VoteReadDto, VoteUpdateDto } from '../../types/ApiTypes';
import { DragDropContext, DropResult } from 'react-beautiful-dnd';
import { Grid } from 'semantic-ui-react';
import { DroppableDateBox } from '../dnd/DroppableDateBox';
import { VotesList } from './VotesList';
import { getGroupedVotes, GroupedVotes } from '../../utils/DtoGroupingUtils';

interface Props {
    votes: VoteReadDto[];
    onUpdate: (voteId: number, vote: VoteUpdateDto) => any;
}
export const Votes = (props: Props) => {
    const { votes, onUpdate } = props;

    const [voteGroups, setVoteGroups] = useState(getGroupedVotes(votes));

    const handleDrop = (result: DropResult) => {
        const destination = result.destination;
        const source = result.source;
        if (!destination) return;
        if (destination.droppableId === source.droppableId && destination.index === source.index) return;

        const sourceGroup = voteGroups[source.droppableId as keyof GroupedVotes];
        const destinationGroup = voteGroups[destination.droppableId as keyof GroupedVotes];

        const vote = sourceGroup[source.index];
        const updatedVote = {
            ...vote,
            status: destination.droppableId.toUpperCase() as "UNANSWERED" | "APPROVED" | "DECLINED",
        };

        sourceGroup.splice(source.index, 1);
        destinationGroup.splice(destination.index, 0, updatedVote);

        const updatedGroups = {
            ...voteGroups,
            [source.droppableId]: sourceGroup,
            [destination.droppableId]: destinationGroup,
        }
        onUpdate(vote.id, { status: updatedVote.status });
        setVoteGroups(updatedGroups);
    }
    return (
        <DragDropContext onDragEnd={handleDrop}>
            <Grid columns={3} textAlign='center' doubling stackable>
                <Grid.Column width='4'>
                    <DroppableDateBox name='Abgelehnt' id='declined' color='red' >
                        <VotesList votes={voteGroups.declined} color={'red'} />
                    </DroppableDateBox>
                </Grid.Column>
                <Grid.Column width='4'>
                    <DroppableDateBox name='Unbeantwortet' id='unanswered' color='blue' >
                        <VotesList votes={voteGroups.unanswered} color={'blue'} />
                    </DroppableDateBox>
                </Grid.Column>
                <Grid.Column width='4'>
                    <DroppableDateBox name='Zugestimmt' id='approved' color='green' >
                        <VotesList votes={voteGroups.approved} color={'green'} />
                    </DroppableDateBox>
                </Grid.Column>
            </Grid>
        </DragDropContext>
    )
}