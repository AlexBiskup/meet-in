import React from 'react';
import { ProjectReadDto } from '../../types/ApiTypes';
import { Divider, Header, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { ProjectMenu } from './ProjectMenu';

interface Props {
    project: ProjectReadDto;
}
export const ProjectHeader = (props: Props) => {
    const { project } = props;
    return (
        <React.Fragment>
            <Divider hidden />
            <Header as='h2'>
                {project.name}
                {project.participantIsOwner && (
                    <Button as={Link} to='/new' floated='right' color='green' content='Neuer Termin' />
                )}
            </Header>
            <Divider hidden />
            <ProjectMenu isOwner={project.participantIsOwner} />
        </React.Fragment >
    )
}