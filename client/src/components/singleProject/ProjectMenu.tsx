import React from 'react';
import { Menu } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

interface Props {
    isOwner: boolean;
}
export const ProjectMenu = (props: Props) => {
    const { isOwner } = props;
    return (
        <Menu widths={isOwner ? 3 : 2}>
            <Menu.Item as={NavLink} to='/appointments' exact={true} >
                Termine
            </Menu.Item>
            <Menu.Item as={NavLink} to='/votes' exact={true} >
                {/*<Label color='blue' floating content={1} circular /> */}
                Abstimmen
            </Menu.Item>
            {isOwner && (
                <Menu.Item as={NavLink} to='/settings' name='Einstellungen' />
            )}
        </Menu>
    )
}