import React, { useState } from 'react';
import { Form, FormGroup, Button } from 'semantic-ui-react';
import { ValidatedInput, isValidEmail } from '../forms/ValidatedInput';
import { ParticipantCreateDto } from '../../types/ApiTypes';

interface Props {
    isPending: boolean;
    onSubmit: (participant: ParticipantCreateDto) => any;
}
export const ParticipantForm = (props: Props) => {
    const { isPending, onSubmit } = props;
    const [emailState, setEmailState] = useState({ value: '', isValid: false });

    const isFormValid = emailState.isValid;

    const handleSubmit = () => {
        if (isFormValid) {
            setEmailState({ value: '', isValid: false })
            onSubmit({ email: emailState.value })
        }
    }

    return (
        <Form loading={isPending} onSubmit={handleSubmit}>
            <FormGroup widths='2'>
                <Form.Field error={!emailState.isValid}>
                    <ValidatedInput placeholder='Teilnehmer Email' rules={[isValidEmail]} inputState={emailState} onChange={setEmailState} />
                </Form.Field>
                <Form.Field>
                    <Button disabled={!isFormValid} color='green'>Hinzufügen</Button>
                </Form.Field>
            </FormGroup>
        </Form>
    )
}