import React from 'react';
import { ParticipantListDto } from '../../types/ApiTypes';
import { Table, Button, Popup } from 'semantic-ui-react';

interface Props {
    participants: ParticipantListDto[];
    onDelete: (participantId: number) => any;
}
export const ParticipantList = (props: Props) => {
    const { participants, onDelete } = props;

    return (
        <Table basic='very' size='small'>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nachname</Table.HeaderCell>
                    <Table.HeaderCell>Vorname</Table.HeaderCell>
                    <Table.HeaderCell>Email</Table.HeaderCell>
                    <Table.HeaderCell textAlign='center'>Aktionen</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {participants.map(
                    (participant, index) => <Row key={index} participant={participant} onDelete={onDelete} />
                )}
            </Table.Body>
        </Table>
    )
}

interface RowProps {
    participant: ParticipantListDto;
    onDelete: (participantId: number) => any;
}
const Row = (props: RowProps) => {
    const { participant: { user, id }, onDelete } = props;
    return (
        <Table.Row>
            <Table.Cell>
                {user.lastName}
            </Table.Cell>
            <Table.Cell>
                {user.firstName}
            </Table.Cell>
            <Table.Cell>
                {user.email}
            </Table.Cell>
            <Table.Cell textAlign='center'>
                <Popup
                    trigger={<Button negative icon='trash' />}
                    content={<Button icon='exclamation' content='Unwiderruflich löschen' onClick={() => onDelete(id)} />}
                    on='click'
                    position='top center'
                />

            </Table.Cell>
        </Table.Row>
    )
}