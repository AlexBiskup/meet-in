import React, { useEffect } from 'react';
import { Input } from './Input';

export type InputState = {
    value: string, isValid: boolean
}
export type Rule = (value: string) => boolean;
interface Props {
    onChange: (inputState: InputState) => void;
    inputState: InputState;
    rules: Array<Rule>;
    placeholder?: string;
    revalidateOn?: string[];
    type?: string;
    autoFocus?: boolean;
}

export const ValidatedInput = (props: Props) => {
    const { onChange, inputState, rules, placeholder, revalidateOn = [], type = 'text', autoFocus = false } = props;

    useEffect(() => handleChange(inputState.value), revalidateOn)

    const handleChange = (value: string) => {
        const isValid = !rules.some(
            rule => !rule(value)
        )
        onChange({ value, isValid, });
    }

    return (
        <Input
            value={inputState.value}
            onChange={handleChange}
            placeholder={placeholder}
            type={type}
            autoFocus={autoFocus}
        />
    )
}

export const isNotEmpty = (value: string): boolean => {
    return value.length > 0;
}

export const isValidEmail = (value: string): boolean => {
    return emailRegex.test(value);
}

export const isMinLength = (value: string, minLength: number): boolean => {
    return value.length >= minLength;
}

export const valueEquals = (value: string, expectedValue: string): boolean => {
    return value === expectedValue;
}

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;