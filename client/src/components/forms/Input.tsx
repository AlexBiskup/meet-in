import React from 'react';

interface Props {
    onChange: (value: string) => void;
    value: string;
    placeholder?: string;
    type?: string;
    autoFocus?: boolean;
}

export const Input = (props: Props) => {
    const { value, onChange, placeholder, type = 'text', autoFocus = false } = props;
    return (
        <input
            value={value}
            onChange={(event) => onChange(event.target.value)}
            placeholder={placeholder}
            type={type}
            autoFocus={autoFocus}
        />
    )
}
