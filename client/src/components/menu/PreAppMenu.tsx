import React from 'react';
import { Menu, Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

interface Props {
    isLoginActive?: boolean;
    isSignupActive?: boolean;
}
export const PreAppMenu = (props: Props) => {
    const { isLoginActive, isSignupActive } = props;
    return (
        <Menu pointing secondary size='large'>
            <Container>
                <Menu.Menu position='right'>
                    <Menu.Item name='Login' as={Link} to='/' active={isLoginActive} />
                    <Menu.Item name='Registrieren' as={Link} to='/signup' active={isSignupActive} />
                </Menu.Menu>
            </Container>
        </Menu>
    )
}