import React from 'react';
import { Menu, Container } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

interface Props {
    onLogout: () => void;
}
export const AppMenu = (props: Props) => {
    const { onLogout } = props;
    return (
        <Menu pointing secondary size='large'>
            <Container>
                <Menu.Item name='Projekte' as={NavLink} to='/' />
                <Menu.Menu position='right'>
                    <Menu.Item
                        name='logout'
                        onClick={onLogout}
                    />
                </Menu.Menu>
            </Container>
        </Menu>
    )
}