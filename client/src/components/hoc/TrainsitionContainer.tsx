import React, { useState, useEffect } from 'react';
import { TransitionGroup } from 'semantic-ui-react';

interface Props {
    children: React.ReactNode;
}
export const TransitionContainer = (props: Props) => {
    const { children } = props;
    const [visible, setVisible] = useState(false);

    useEffect(() => setVisible(true), []);

    return (
        <TransitionGroup duration={500} animation='fade' >
            {visible && children}
        </TransitionGroup>
    )
}