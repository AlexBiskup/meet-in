import { Rest as GenericRest, RestProps, Execute } from 'ts-rest-react';
import { Loader, Container, Divider } from 'semantic-ui-react';
import React from 'react';
import { TransitionContainer } from './TrainsitionContainer';

interface Props<T, A extends any[]> extends RestProps<T, A> {
    children?: (data: T, execute: Execute<A>) => JSX.Element
}
export const Rest = <T, A extends any[]>(props: Props<T, A>) => {

    const { children, render, ...rest } = props;
    const usedRenderFunction = children ? children : render;

    return <GenericRest
        {...rest}
        render={usedRenderFunction}
        renderOnNoData={() => <React.Fragment />}
        renderOnLoading={() => loader}
        renderOnError={() => error}
    />
}


const loader = (
    <div>
        <Divider hidden />
        <Loader active inline='centered' />
        <Divider hidden />
    </div>
)

const error = (
    <TransitionContainer>
        <Container textAlign='center'>
            Es ist ein Fehler aufgetreten.
        </Container>
    </TransitionContainer>
)
