import { useState } from "react";

export function useLocalStorage(key: string): [string | null, (value: string) => void, () => void] {
    const persistedValue = localStorage.getItem(key);
    const [value, setValue] = useState(persistedValue);
    const setPersistedValue = (value: string) => {
        localStorage.setItem(key, value);
        setValue(value);
    }
    const reset = () => {
        localStorage.removeItem(key);
        setValue(null);
    }
    return [value, setPersistedValue, reset];
}