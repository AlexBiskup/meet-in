import { useRest as useGenericRest, SuccessCallback, ErrorCallback, Result } from 'ts-rest-react';
import { getErrorDetails } from '../utils/ErrorResolver';
import { notifyError, notifyInfo } from '../utils/Notifications';
import { useContext } from 'react';
import { LogoutContext } from '../containers/App';


export function useRest<T, A extends any[]>(request: (...args: A) => Promise<T>, successCallback?: SuccessCallback<T>, errorCallback?: ErrorCallback, isLogin = false): Result<T, A> {
    const logout = useContext(LogoutContext);
    const onError = (error: any) => {
        if (errorCallback) {
            errorCallback(error);
        }
        const errorDetails = getErrorDetails(error);
        if (errorDetails.statusCode === 401 && !isLogin) {
            logout();
            notifyInfo('Die Sitzung ist abgelaufen');
        } else {
            notifyError(errorDetails.message || 'Ein Fehler ist aufgetreten');
        }
    }
    return useGenericRest(request, successCallback, onError);
}

