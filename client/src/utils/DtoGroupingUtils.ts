import { AppointmentReadDto, VoteReadDto } from "../types/ApiTypes";

export interface GroupedAppointments {
    fixed: AppointmentReadDto[],
    reserved: AppointmentReadDto[],
    open: AppointmentReadDto[],
    dismissed: AppointmentReadDto[]
}

export function getGroupedAppointments(appointments: AppointmentReadDto[]): GroupedAppointments {
    const fixed: AppointmentReadDto[] = [];
    const reserved: AppointmentReadDto[] = [];
    const open: AppointmentReadDto[] = [];
    const dismissed: AppointmentReadDto[] = [];

    appointments.forEach(
        appointment => {
            switch (appointment.status) {
                case 'FIXED': fixed.push(appointment); break;
                case 'RESERVED': reserved.push(appointment); break;
                case 'OPEN': open.push(appointment); break;
                case 'DISMISSED': dismissed.push(appointment); break;
            }
        }
    )

    return { fixed, reserved, open, dismissed };

}

export interface GroupedVotes {
    unanswered: VoteReadDto[];
    approved: VoteReadDto[];
    declined: VoteReadDto[];
}
export function getGroupedVotes(votes: VoteReadDto[]): GroupedVotes {

    const unanswered: VoteReadDto[] = [];
    const approved: VoteReadDto[] = [];
    const declined: VoteReadDto[] = [];

    votes.forEach(
        vote => {
            switch (vote.status) {
                case 'UNANSWERED': unanswered.push(vote); break;
                case 'APPROVED': approved.push(vote); break;
                case 'DECLINED': declined.push(vote); break;
            }
        }
    )
    return { unanswered, approved, declined }
}