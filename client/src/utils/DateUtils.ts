import moment from 'moment';

const DATE_FORMAT = 'DD.MM.YY'
const TIME_FORMAT = 'HH:mm'
const DATE_TIME_FORMAT = `${DATE_FORMAT} ${TIME_FORMAT}`

export function formatDate(date: string): string {
    return moment(date).format(DATE_TIME_FORMAT);
}

export function getFormattedStartAndEnd(start: string, end: string) {
    const startMoment = moment(start);
    const endMoment = moment(end);

    if (startMoment.isSame(endMoment, 'day')) {
        return {
            start: startMoment.format(DATE_TIME_FORMAT),
            end: endMoment.format(TIME_FORMAT)
        }
    }

    return {
        start: startMoment.format(DATE_TIME_FORMAT),
        end: endMoment.format(DATE_TIME_FORMAT)
    }
}