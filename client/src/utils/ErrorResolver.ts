
interface ErrorDetails {
    message?: string;
    statusCode?: number;
}

export function getErrorDetails(error: any): ErrorDetails {
    if (!error) {
        throw new Error('No error present.');
    }
    const details: ErrorDetails = {};

    const response = error.response;

    if (response) {
        details.message = response.data.message;
        details.statusCode = response.status;
    } else {
        details.message = error.message;
    }
    return details;
}