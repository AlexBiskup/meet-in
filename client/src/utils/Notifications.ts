import { toast } from 'react-toastify';
import { css } from 'glamor';

export function notifySuccess(message: string) {
    toast(
        message,
        {
            hideProgressBar: true,
            autoClose: 6000,
            className: css({ borderLeft: `${borderWidth} solid ${successColor}` }),
            bodyClassName: css({ color: successColor })
        }
    );
}

export function notifyInfo(message: string) {
    toast(
        message,
        {
            hideProgressBar: true,
            autoClose: 6000,
            className: css({ borderLeft: `${borderWidth} solid ${infoColor}` }),
            bodyClassName: css({ color: infoColor })
        }
    );
}

export function notifyError(message: string) {
    toast(
        message,
        {
            hideProgressBar: true,
            autoClose: false,
            className: css({ borderLeft: `${borderWidth} solid ${errorColor}` }),
            bodyClassName: css({ color: errorColor })
        }
    );
}

const borderWidth = '12px'

const successColor = '#21ba45';
const errorColor = '#db2828';
const infoColor = '#2185d0';