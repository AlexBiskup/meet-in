import React from 'react';
import { Switch, Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { Login } from '../containers/Login';
import { PreAppMenu } from '../components/menu/PreAppMenu';
import { Divider, Container } from 'semantic-ui-react';
import { Signup } from '../containers/Signup';

interface Props {
    setAuthToken: (authToken: string) => void;
}

export const PreAppRoutes = (props: Props) => {
    const { setAuthToken } = props;

    return (
        <BrowserRouter>
            <Switch>
                <Route path='/signup' render={(props) => <Signup {...props} />} />
                <Route path='/' render={() => <Login setAuthToken={setAuthToken} />} />
            </Switch>
        </BrowserRouter>

    )

}

export default PreAppRoutes;