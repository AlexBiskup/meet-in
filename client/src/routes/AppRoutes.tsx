import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { AppMenu } from '../components/menu/AppMenu';
import { Container, Divider } from 'semantic-ui-react';
import { Projects } from '../containers/Projects';

interface Props {
    logout: () => void;
}
export const AppRoutes = (props: Props) => {
    const { logout } = props;
    return (
        <BrowserRouter>
            <AppMenu onLogout={logout} />
            <Divider hidden />
            <Container>
                <Switch>
                    <Route path='/projects' render={(props) => <Projects {...props} />} />
                    <Redirect to='/projects' />
                </Switch>
            </Container>
        </BrowserRouter>
    )
}

export default AppRoutes;
