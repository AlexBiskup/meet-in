FROM gradle:6.3.0-jdk8 as builder
WORKDIR /home/gradle/project
ADD  ./ ./
RUN gradle clean bootJar

FROM openjdk:8-alpine
COPY --from=builder /home/gradle/project/build/libs/meet-in.jar app.jar
EXPOSE 8080
CMD java -jar app.jar

