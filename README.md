# Meet-in application

## REST-API
### Environment
This application expects a MySql database to be available at localhost:3306. To start the database in a docker container run `docker-compose -f docker-compose-dev.yml up`

### Building
To build the application run `./gradlew clean bootJar`

### Starting
To start the application run `java -jar build/libs/meet-in.jar`

### Documentation
Visit http://localhost:8080/swagger-ui.html for the REST-API documentation.

## Web-client
Working directory: `./client`

### Setup
Run `npm install` to install all dependencies.

### Starting
Run `npm start` to start the application. <br>
Visit http://localhost:3000


