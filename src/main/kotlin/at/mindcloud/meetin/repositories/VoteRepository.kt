package at.mindcloud.meetin.repositories

import at.mindcloud.meetin.entities.Vote
import at.mindcloud.meetin.entities.VoteStatus
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface VoteRepository : CrudRepository<Vote, Long> {

    @Query(
        nativeQuery = true,
        value = "SELECT * FROM vote WHERE participant_id = :participantId AND appointment_id IN " +
                "(SELECT id FROM appointment WHERE `status` = 'OPEN')"
    )
    fun findAllByParticipantId(@Param("participantId") participantId: Long): List<Vote>

    fun countAllByAppointmentIdAndStatusEquals(appointmentId: Long, status: VoteStatus): Long

    fun countAllByAppointmentId(appointmentId: Long): Long

    fun findAllByAppointmentId(appointmentId: Long): List<Vote>

}
