package at.mindcloud.meetin.repositories

import at.mindcloud.meetin.entities.Appointment
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AppointmentRepository : CrudRepository<Appointment, Long> {

    fun getAllByProjectIdOrderByStartAsc(projectId: Long): List<Appointment>

}
