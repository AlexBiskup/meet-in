package at.mindcloud.meetin.repositories

import at.mindcloud.meetin.entities.Participant
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ParticipantRepository : CrudRepository<Participant, Long> {

    fun findAllByUserId(userId: Long): List<Participant>

    fun findByProjectIdAndUserId(projectId: Long, userId: Long): Optional<Participant>

    fun findAllByProjectId(projectId: Long): List<Participant>

}
