package at.mindcloud.meetin.repositories

import at.mindcloud.meetin.entities.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : CrudRepository<User, Long> {

    fun findByEmail(email: String): Optional<User>

}
