package at.mindcloud.meetin.repositories

import at.mindcloud.meetin.entities.Project
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProjectRepository : CrudRepository<Project, Long> {

    fun getOneById(id: Long): Project

}
