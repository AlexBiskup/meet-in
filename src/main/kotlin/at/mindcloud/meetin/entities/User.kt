package at.mindcloud.meetin.entities

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@Entity
data class User(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long = -1,
    var firstName: String = "",
    var lastName: String = "",
    var email: String = "",
    var password: String = ""
)
