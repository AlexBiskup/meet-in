package at.mindcloud.meetin.entities

import at.mindcloud.meetin.entities.AppointmentStatus.OPEN
import java.util.*
import javax.persistence.Entity
import javax.persistence.EnumType.STRING
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@Entity
data class Appointment(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long = -1,
    var projectId: Long = -1,
    var start: Date = Date(),
    var end: Date = Date(),
    @Enumerated(STRING)
    var status: AppointmentStatus = OPEN,
    var name: String? = null
)

enum class AppointmentStatus {
    OPEN, RESERVED, FIXED, DISMISSED
}
