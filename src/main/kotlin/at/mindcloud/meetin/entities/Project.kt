package at.mindcloud.meetin.entities

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@Entity
class Project(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long = -1,
    var ownerId: Long = -1,
    var name: String = "",
    var autoDismiss: Boolean = false,
    var autoFixing: Boolean = false
)
