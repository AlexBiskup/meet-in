package at.mindcloud.meetin.entities

import at.mindcloud.meetin.entities.VoteStatus.UNANSWERED
import javax.persistence.Entity
import javax.persistence.EnumType.STRING
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@Entity
data class Vote(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long = -1,
    val appointmentId: Long,
    val participantId: Long,
    @Enumerated(STRING)
    var status: VoteStatus = UNANSWERED
)

enum class VoteStatus {
    UNANSWERED, APPROVED, DECLINED
}
