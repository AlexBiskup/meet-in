package at.mindcloud.meetin.entities

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@Entity
class Participant(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long = -1,
    val projectId: Long = -1,
    val userId: Long = -1
)
