package at.mindcloud.meetin.entities.dto

import at.mindcloud.meetin.entities.AppointmentStatus
import at.mindcloud.meetin.entities.AppointmentStatus.OPEN
import java.util.*

data class AppointmentCreateDto(
    val projectId: Long,
    val start: Date,
    val end: Date,
    val name: String?
)

data class AppointmentReadDto(
    var id: Long = -1,
    var start: Date = Date(),
    var end: Date = Date(),
    var status: AppointmentStatus = OPEN,
    var name: String? = null,
    var voteStats: VoteStatsDto = VoteStatsDto()
)

data class AppointmentDetailsDto(
    var id: Long = -1,
    var start: Date = Date(),
    var end: Date = Date(),
    var status: AppointmentStatus = OPEN,
    var name: String? = null,
    var voteStats: VoteStatsDto = VoteStatsDto(),
    var votes: List<VoteEmbeddedDto> = listOf()
)

data class AppointmentEmbeddedDto(
    var id: Long = -1,
    var start: Date = Date(),
    var end: Date = Date(),
    var status: AppointmentStatus = OPEN,
    var name: String? = null
)

data class AppointmentUpdateDto(
    val status: AppointmentStatus,
    val start: Date,
    val end: Date,
    val name: String?
)


data class VoteStatsDto(
    var voteCount: Long = 0,
    var declinedCount: Long = 0,
    var approvedCount: Long = 0
)
