package at.mindcloud.meetin.entities.dto

data class ParticipantCreateDto(
    val email: String
)

data class ParticipantListDto(
    var id: Long = -1,
    var user: UserReadDto = UserReadDto()
)
