package at.mindcloud.meetin.entities.dto

import at.mindcloud.meetin.entities.VoteStatus
import at.mindcloud.meetin.entities.VoteStatus.UNANSWERED

data class VoteReadDto(
    var id: Long = -1,
    var appointment: AppointmentReadDto = AppointmentReadDto(),
    var participantId: Long = -1,
    var status: VoteStatus = UNANSWERED
)

data class VoteEmbeddedDto(
    var id: Long = -1,
    var participant: ParticipantListDto = ParticipantListDto(),
    var status: VoteStatus = UNANSWERED
)

data class VoteUpdateDto(
    val status: VoteStatus
)
