package at.mindcloud.meetin.entities.dto

data class ProjectCreateDto(
    val name: String,
    val autoDismiss: Boolean,
    val autoFixing: Boolean
)

data class ProjectUpdateDto(
    val name: String,
    val autoDismiss: Boolean,
    val autoFixing: Boolean
)

data class ProjectReadDto(
    var id: Long = -1,
    var name: String = "",
    var participantIsOwner: Boolean = false,
    var owner: UserReadDto = UserReadDto(),
    val autoDismiss: Boolean,
    val autoFixing: Boolean
)

