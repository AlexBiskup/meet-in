package at.mindcloud.meetin.entities.dto

import at.mindcloud.meetin.entities.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class UserCreateDto(
    val firstName: String,
    val lastName: String,
    val email: String,
    val password: String
)

data class UserReadDto(
    var firstName: String = "",
    var lastName: String = "",
    var email: String = ""
)

class UserAuthDto(
    val user: User
) : UserDetails {

    override fun getUsername(): String {
        return user.email
    }

    override fun getPassword(): String {
        return user.password
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return mutableListOf()
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

}
