package at.mindcloud.meetin.utilities

import at.mindcloud.meetin.entities.dto.UserAuthDto
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

@Component
class ControllerUtilities {

    fun getUserIdByAuthentication(authentication: Authentication): Long {
        val userDto = authentication.principal as UserAuthDto
        return userDto.user.id
    }

}
