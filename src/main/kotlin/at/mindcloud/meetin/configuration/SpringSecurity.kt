package at.mindcloud.meetin.configuration

import at.mindcloud.tokenauth.configuration.TokenAuthenticationSecurityAdapter
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
class SpringSecurity: TokenAuthenticationSecurityAdapter()
