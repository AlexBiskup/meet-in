package at.mindcloud.meetin.services

import at.mindcloud.meetin.entities.Participant
import at.mindcloud.meetin.entities.Project
import at.mindcloud.meetin.entities.dto.ProjectCreateDto
import at.mindcloud.meetin.entities.dto.ProjectUpdateDto
import at.mindcloud.meetin.mappers.ProjectMapper
import at.mindcloud.meetin.repositories.ProjectRepository
import org.springframework.stereotype.Service

interface ProjectService {

    fun createProject(projectDto: ProjectCreateDto, userId: Long): Long

    fun getProject(projectId: Long): Project

    fun updateProject(projectId: Long, updatedProject: ProjectUpdateDto)

}

@Service
class ProjectServiceImpl(
    private val projectRepository: ProjectRepository,
    private val participantService: ParticipantService,
    private val projectMapper: ProjectMapper
) : ProjectService {

    override fun updateProject(projectId: Long, updatedProject: ProjectUpdateDto) {
        val project = getProject(projectId)
        projectMapper.updateDtoToEntity(updatedProject, project)
        saveProject(project)
    }


    override fun getProject(projectId: Long): Project {
        return projectRepository.getOneById(projectId)
    }

    override fun createProject(projectDto: ProjectCreateDto, userId: Long): Long {
        val project = projectMapper.createDtoToEntity(projectDto, userId)
        val projectId = saveProject(project)
        val participant = Participant(id = -1, projectId = projectId, userId = userId)
        participantService.saveParticipant(participant)
        return projectId
    }

    private fun saveProject(project: Project): Long {
        return projectRepository.save(project).id
    }

}
