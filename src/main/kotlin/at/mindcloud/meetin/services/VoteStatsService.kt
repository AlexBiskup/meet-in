package at.mindcloud.meetin.services

import at.mindcloud.meetin.entities.VoteStatus
import at.mindcloud.meetin.entities.dto.VoteStatsDto
import at.mindcloud.meetin.repositories.VoteRepository
import org.springframework.stereotype.Service

interface VoteStatsService {

    fun getVoteStatsForAppointment(appointmentId: Long): VoteStatsDto

}

@Service
class VoteStatServiceImpl(
    private val voteRepository: VoteRepository
) : VoteStatsService {

    override fun getVoteStatsForAppointment(appointmentId: Long): VoteStatsDto {
        return VoteStatsDto(
            voteCount = voteRepository.countAllByAppointmentId(appointmentId),
            declinedCount = voteRepository.countAllByAppointmentIdAndStatusEquals(
                appointmentId,
                VoteStatus.DECLINED
            ),
            approvedCount = voteRepository.countAllByAppointmentIdAndStatusEquals(
                appointmentId,
                VoteStatus.APPROVED
            )
        )
    }

}
