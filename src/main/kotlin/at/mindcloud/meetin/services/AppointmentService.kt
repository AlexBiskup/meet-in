package at.mindcloud.meetin.services

import at.mindcloud.meetin.entities.Appointment
import at.mindcloud.meetin.entities.dto.AppointmentCreateDto
import at.mindcloud.meetin.entities.dto.AppointmentUpdateDto
import at.mindcloud.meetin.mappers.AppointmentMapper
import at.mindcloud.meetin.repositories.AppointmentRepository
import org.springframework.stereotype.Service

interface AppointmentService {

    fun createAppointment(appointmentDto: AppointmentCreateDto): Long

    fun getAppointmentsByProjectId(projectId: Long): List<Appointment>

    fun update(appointmentId: Long, updatedAppointment: AppointmentUpdateDto)

    fun getById(appointmentId: Long): Appointment

    fun delete(appointmentId: Long)

}

@Service
class AppointmentServiceImpl(
    private val appointmentMapper: AppointmentMapper,
    private val appointmentRepository: AppointmentRepository,
    private val voteService: VoteService
) : AppointmentService {

    override fun delete(appointmentId: Long) {
        appointmentRepository.deleteById(appointmentId)
    }

    override fun update(appointmentId: Long, updatedAppointment: AppointmentUpdateDto) {
        val appointment = getById(appointmentId)
        appointmentMapper.updateDtoToEntity(updatedAppointment, appointment)
        saveAppointment(appointment)
    }

    override fun getById(appointmentId: Long): Appointment {
        val appointment = appointmentRepository.findById(appointmentId)
        if (appointment.isPresent) {
            return appointment.get()
        }
        throw IllegalArgumentException("Appointment with id $appointmentId could not be found")
    }


    override fun getAppointmentsByProjectId(projectId: Long): List<Appointment> {
        return appointmentRepository.getAllByProjectIdOrderByStartAsc(projectId)
    }

    override fun createAppointment(appointmentDto: AppointmentCreateDto): Long {
        val appointment = appointmentMapper.createDtoToEntity(appointmentDto)
        val savedAppointment = saveAppointment(appointment)
        voteService.createVotesForAppointment(savedAppointment)
        return savedAppointment.id
    }

    private fun saveAppointment(appointment: Appointment): Appointment {
        return appointmentRepository.save(appointment)
    }

}
