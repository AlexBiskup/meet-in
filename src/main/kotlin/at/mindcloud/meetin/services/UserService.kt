package at.mindcloud.meetin.services

import at.mindcloud.meetin.entities.User
import at.mindcloud.meetin.entities.dto.UserAuthDto
import at.mindcloud.meetin.entities.dto.UserCreateDto
import at.mindcloud.meetin.mappers.UserMappingService
import at.mindcloud.meetin.repositories.UserRepository
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

interface UserService : UserDetailsService {

    fun createUser(userDto: UserCreateDto): Long

    fun getUserByEmail(email: String): User

    fun getById(userId: Long): User
}

@Service
class UserServiceImpl(
    private val userMappingService: UserMappingService,
    private val userRepository: UserRepository
) : UserService {

    override fun getById(userId: Long): User {
        val user = userRepository.findById(userId)
        if (user.isPresent) {
            return user.get()
        }
        throw IllegalArgumentException("User with id $userId could not be found.")
    }

    override fun getUserByEmail(email: String): User {
        val user = userRepository.findByEmail(email)
        if (user.isPresent) {
            return user.get()
        }
        throw IllegalArgumentException("User with username '$email' could not be found.")
    }

    override fun createUser(userDto: UserCreateDto): Long {
        val user = userMappingService.mapCreateDtoToEntity(userDto)
        return saveUser(user)
    }

    override fun loadUserByUsername(username: String): UserDetails {
        return UserAuthDto(getUserByEmail(username))
    }

    private fun saveUser(user: User): Long {
        try {
            return userRepository.save(user).id
        } catch (exception: DataIntegrityViolationException) {
            throw IllegalArgumentException("User with username ${user.email} already exists.")
        }
    }

}
