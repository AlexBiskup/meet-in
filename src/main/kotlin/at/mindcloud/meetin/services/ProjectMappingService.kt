package at.mindcloud.meetin.services

import at.mindcloud.meetin.entities.Participant
import at.mindcloud.meetin.entities.Project
import at.mindcloud.meetin.entities.dto.ProjectCreateDto
import at.mindcloud.meetin.entities.dto.ProjectReadDto
import at.mindcloud.meetin.mappers.ProjectMapper
import at.mindcloud.meetin.mappers.UserMapper
import org.springframework.stereotype.Service

interface ProjectMappingService {

    fun entityToDto(participant: Participant): ProjectReadDto

    fun createDtoToEntity(project: ProjectCreateDto, userId: Long): Project

}

@Service
class ProjectMappingServiceImpl(
    private val projectService: ProjectService,
    private val projectMapper: ProjectMapper,
    private val userService: UserService,
    private val userMapper: UserMapper
) : ProjectMappingService {

    override fun createDtoToEntity(project: ProjectCreateDto, userId: Long): Project {
        return projectMapper.createDtoToEntity(project, userId)
    }

    override fun entityToDto(participant: Participant): ProjectReadDto {
        val project = projectService.getProject(participant.projectId)
        val user = userService.getById(project.ownerId)
        val userDto = userMapper.entityToReadDto(user)
        val isOwner = participant.userId == project.ownerId
        return ProjectReadDto(
            project.id,
            project.name,
            isOwner,
            userDto,
            project.autoDismiss,
            project.autoFixing
        )
    }

}
