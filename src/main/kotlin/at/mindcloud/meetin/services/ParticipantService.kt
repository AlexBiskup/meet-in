package at.mindcloud.meetin.services

import at.mindcloud.meetin.entities.Participant
import at.mindcloud.meetin.entities.Project
import at.mindcloud.meetin.entities.dto.ParticipantCreateDto
import at.mindcloud.meetin.repositories.ParticipantRepository
import at.mindcloud.meetin.repositories.ProjectRepository
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service

interface ParticipantService {

    fun getParticipantsByUserId(userId: Long): List<Participant>

    fun getParticipantsByProjectId(projectId: Long): List<Participant>;

    fun saveParticipant(participant: Participant): Participant

    fun createParticipant(participantDto: ParticipantCreateDto, projectId: Long): Long

    fun getParticipantByProjectIdAndUserId(projectId: Long, userId: Long): Participant

    fun deleteParticipant(participantId: Long)

    fun getById(participantId: Long): Participant

}

@Service
class ParticipantServiceImpl(
    private val participantRepository: ParticipantRepository,
    private val userService: UserService,
    private val projectRepository: ProjectRepository,
    private val voteService: VoteService
) : ParticipantService {

    override fun getById(participantId: Long): Participant {
        val participant = participantRepository.findById(participantId)
        if (participant.isPresent) {
            return participant.get()
        }
        throw IllegalArgumentException("Participant with id $participantId could not be found")
    }

    override fun deleteParticipant(participantId: Long) {
        val participant = getById(participantId)
        val project = projectRepository.findById(participant.projectId).get()
        if (participantIsNotOwnerOfProject(participant, project)) {
            participantRepository.deleteById(participantId)
        } else {
            throw IllegalArgumentException("The owner of the project can not be deleted")
        }
    }

    override fun createParticipant(participantDto: ParticipantCreateDto, projectId: Long): Long {
        val user = userService.getUserByEmail(participantDto.email)
        val participant = Participant(projectId = projectId, userId = user.id)
        val savedParticipant = saveParticipant(participant)
        voteService.createVotesForParticipant(savedParticipant)
        return savedParticipant.id
    }

    override fun getParticipantsByProjectId(projectId: Long): List<Participant> {
        return participantRepository.findAllByProjectId(projectId)
    }

    override fun getParticipantByProjectIdAndUserId(projectId: Long, userId: Long): Participant {
        val participant = participantRepository.findByProjectIdAndUserId(projectId, userId)
        if (participant.isPresent) return participant.get()
        throw IllegalArgumentException("The project with the id >$projectId< could not be found.")
    }

    override fun getParticipantsByUserId(userId: Long): List<Participant> {
        return participantRepository.findAllByUserId(userId)
    }

    override fun saveParticipant(participant: Participant): Participant {
        try {
            return participantRepository.save(participant)
        } catch (exception: DataIntegrityViolationException) {
            throw IllegalArgumentException(exception.message)
        }
    }

    private fun participantIsNotOwnerOfProject(
        participant: Participant,
        project: Project
    ): Boolean {
        return participant.userId != project.ownerId
    }
}
