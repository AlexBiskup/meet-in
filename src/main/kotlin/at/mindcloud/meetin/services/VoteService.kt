package at.mindcloud.meetin.services

import at.mindcloud.meetin.entities.Appointment
import at.mindcloud.meetin.entities.AppointmentStatus.DISMISSED
import at.mindcloud.meetin.entities.AppointmentStatus.RESERVED
import at.mindcloud.meetin.entities.Participant
import at.mindcloud.meetin.entities.Vote
import at.mindcloud.meetin.entities.dto.VoteStatsDto
import at.mindcloud.meetin.entities.dto.VoteUpdateDto
import at.mindcloud.meetin.mappers.VoteMapper
import at.mindcloud.meetin.repositories.AppointmentRepository
import at.mindcloud.meetin.repositories.ParticipantRepository
import at.mindcloud.meetin.repositories.ProjectRepository
import at.mindcloud.meetin.repositories.VoteRepository
import org.springframework.stereotype.Service
import kotlin.concurrent.thread

interface VoteService {

    fun createVotesForParticipant(participant: Participant): List<Long>

    fun createVotesForAppointment(appointment: Appointment): List<Long>

    fun getVotesForParticipant(participantId: Long): List<Vote>

    fun getVotes(appointmentId: Long): List<Vote>

    fun updateVote(voteId: Long, updatedVote: VoteUpdateDto)

    fun getById(voteId: Long): Vote
}

@Service
class VoteServiceImpl(
    private val appointmentRepository: AppointmentRepository,
    private val voteRepository: VoteRepository,
    private val participantRepository: ParticipantRepository,
    private val projectRepository: ProjectRepository,
    private val voteMapper: VoteMapper,
    private val voteStatsService: VoteStatsService
) : VoteService {

    override fun getVotes(appointmentId: Long): List<Vote> {
        return voteRepository.findAllByAppointmentId(appointmentId)
    }

    override fun getById(voteId: Long): Vote {
        val vote = voteRepository.findById(voteId)
        if (vote.isPresent) {
            return vote.get()
        }
        throw IllegalArgumentException("Vote with id $voteId could not be found.")
    }

    override fun updateVote(voteId: Long, updatedVote: VoteUpdateDto) {
        val vote = getById(voteId)
        voteMapper.updateDtoToEntity(updatedVote, vote)
        saveVote(vote)
        thread(start = true) {
            updateAppointmentStatus(vote)
        }
    }

    override fun getVotesForParticipant(participantId: Long): List<Vote> {
        return voteRepository.findAllByParticipantId(participantId)
    }

    override fun createVotesForParticipant(participant: Participant): List<Long> {
        val appointments =
            appointmentRepository.getAllByProjectIdOrderByStartAsc(participant.projectId)
        val votes = appointments.map { Vote(appointmentId = it.id, participantId = participant.id) }
        return saveAll(votes)
    }

    override fun createVotesForAppointment(appointment: Appointment): List<Long> {
        val participants = participantRepository.findAllByProjectId(appointment.projectId)
        val votes = participants.map { Vote(appointmentId = appointment.id, participantId = it.id) }
        return saveAll(votes)
    }

    private fun saveVote(vote: Vote): Vote {
        return voteRepository.save(vote)
    }

    private fun saveAll(votes: List<Vote>): List<Long> {
        return voteRepository.saveAll(votes).map { it.id }
    }

    private fun updateAppointmentStatus(vote: Vote) {
        val participant = participantRepository.findById(vote.participantId).get()
        val appointment = appointmentRepository.findById(vote.appointmentId).get()
        val voteStats = voteStatsService.getVoteStatsForAppointment(appointment.id)
        val project = projectRepository.getOneById(participant.projectId)
        if (project.autoFixing && allVotesAreStatusApproved(voteStats)) {
            appointment.status = RESERVED
            appointmentRepository.save(appointment)
        }
        if (project.autoDismiss && anyVoteIsDeclined(voteStats)) {
            appointment.status = DISMISSED
            appointmentRepository.save(appointment)
        }
    }

    private fun allVotesAreStatusApproved(voteStats: VoteStatsDto): Boolean {
        return voteStats.approvedCount == voteStats.voteCount
    }

    private fun anyVoteIsDeclined(voteStats: VoteStatsDto): Boolean {
        return voteStats.declinedCount > 0
    }

}
