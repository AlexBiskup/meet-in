package at.mindcloud.meetin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages = ["at.mindcloud"])
class MeetInApplication

fun main(args: Array<String>) {
	runApplication<MeetInApplication>(*args)
}
