package at.mindcloud.meetin.mappers

import at.mindcloud.meetin.entities.Appointment
import at.mindcloud.meetin.entities.dto.*
import at.mindcloud.meetin.services.VoteService
import at.mindcloud.meetin.services.VoteStatsService
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings
import org.springframework.stereotype.Component

@Mapper(uses = [VoteMapper::class])
abstract class AppointmentMapper {

    @Mappings(
        Mapping(target = "status", ignore = true)
    )
    abstract fun createDtoToEntity(dto: AppointmentCreateDto): Appointment

    @Mapping(target = "voteStats", source = "voteStatsDto")
    abstract fun entityToReadDto(
        entity: Appointment,
        voteStatsDto: VoteStatsDto
    ): AppointmentReadDto

    @Mappings(
        Mapping(target = "voteStats", source = "voteStatsDto"),
        Mapping(target = "votes", source = "votes")
    )
    abstract fun entityToDetailsDto(
        entity: Appointment,
        voteStatsDto: VoteStatsDto,
        votes: List<VoteEmbeddedDto>
    ): AppointmentDetailsDto

    abstract fun entityToEmbeddedDto(entity: Appointment): AppointmentEmbeddedDto

    @Mappings(
        Mapping(target = "projectId", ignore = true)
    )
    abstract fun updateDtoToEntity(updateDto: AppointmentUpdateDto, @MappingTarget entity: Appointment)

}

@Component
class AppointmentMappingHelper(
    private val appointmentMapper: AppointmentMapper,
    private val voteStatsService: VoteStatsService,
    private val votesService: VoteService,
    private val voteMappingService: VoteMappingService
) {

    fun entityToReadDto(entity: Appointment): AppointmentReadDto {
        val voteStats = voteStatsService.getVoteStatsForAppointment(entity.id)
        return appointmentMapper.entityToReadDto(entity, voteStats)
    }

    fun entityToDetailsDto(entity: Appointment): AppointmentDetailsDto {
        val voteStats = voteStatsService.getVoteStatsForAppointment(entity.id)
        val votes = votesService.getVotes(entity.id).map { voteMappingService.entityToEmbeddedDto(it) }
        return appointmentMapper.entityToDetailsDto(entity, voteStats, votes)
    }

}
