package at.mindcloud.meetin.mappers

import at.mindcloud.meetin.entities.Participant
import at.mindcloud.meetin.entities.User
import at.mindcloud.meetin.entities.dto.ParticipantListDto
import at.mindcloud.meetin.services.UserService
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.springframework.stereotype.Service

interface ParticipantMappingService {

    fun entityToListDto(entity: Participant): ParticipantListDto

}

@Service
class ParticipantMappingServiceImpl(
    private val userService: UserService,
    private val participantMapper: ParticipantMapper
) : ParticipantMappingService {

    override fun entityToListDto(entity: Participant): ParticipantListDto {
        val user = userService.getById(entity.userId)
        return participantMapper.entityToListDto(entity, user)
    }

}

@Mapper(uses = [UserMapper::class])
abstract class ParticipantMapper {

    @Mappings(
        Mapping(target = "id", source = "entity.id"),
        Mapping(target = "user", source = "user")
    )
    abstract fun entityToListDto(entity: Participant, user: User): ParticipantListDto

}
