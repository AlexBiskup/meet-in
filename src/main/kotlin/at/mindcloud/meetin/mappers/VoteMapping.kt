package at.mindcloud.meetin.mappers

import at.mindcloud.meetin.entities.Appointment
import at.mindcloud.meetin.entities.Vote
import at.mindcloud.meetin.entities.dto.*
import at.mindcloud.meetin.services.AppointmentService
import at.mindcloud.meetin.services.ParticipantService
import at.mindcloud.meetin.services.VoteStatsService
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings
import org.springframework.stereotype.Service

interface VoteMappingService {

    fun entityToReadDto(entity: Vote): VoteReadDto

    fun updateDtoToEntity(updateDto: VoteUpdateDto, entity: Vote)

    fun entityToEmbeddedDto(entity: Vote): VoteEmbeddedDto

}

@Service
class VoteMappingServiceImpl(
    private val voteMapper: VoteMapper,
    private val appointmentService: AppointmentService,
    private val voteStatsService: VoteStatsService,
    private val participantService: ParticipantService,
    private val participantMappingService: ParticipantMappingService
) : VoteMappingService {

    override fun updateDtoToEntity(updateDto: VoteUpdateDto, entity: Vote) {
        voteMapper.updateDtoToEntity(updateDto, entity)
    }

    override fun entityToReadDto(entity: Vote): VoteReadDto {
        val appointment = appointmentService.getById(entity.appointmentId)
        val voteStats = voteStatsService.getVoteStatsForAppointment(appointment.id)
        return voteMapper.entityToReadDto(entity, appointment, voteStats)
    }

    override fun entityToEmbeddedDto(entity: Vote): VoteEmbeddedDto {
        val participant = participantService.getById(entity.participantId)
        return voteMapper.entityToEmbeddedDto(entity, participantMappingService.entityToListDto(participant))
    }


}

@Mapper(uses = [AppointmentMapper::class])
abstract class VoteMapper {

    @Mappings(
        Mapping(target = "appointment", source = "appointment"),
        Mapping(target = "id", source = "entity.id"),
        Mapping(target = "status", source = "entity.status"),
        Mapping(target = "appointment.voteStats", source = "voteStats")
    )
    abstract fun entityToReadDto(
        entity: Vote,
        appointment: Appointment,
        voteStats: VoteStatsDto
    ): VoteReadDto

    @Mappings(
        Mapping(target = "participant", source = "participant"),
        Mapping(target = "id", source = "entity.id")
    )
    abstract fun entityToEmbeddedDto(entity: Vote, participant: ParticipantListDto): VoteEmbeddedDto

    abstract fun updateDtoToEntity(updateDto: VoteUpdateDto, @MappingTarget entity: Vote)

}
