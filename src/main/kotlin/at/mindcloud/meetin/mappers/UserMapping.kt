package at.mindcloud.meetin.mappers

import at.mindcloud.meetin.entities.User
import at.mindcloud.meetin.entities.dto.UserCreateDto
import at.mindcloud.meetin.entities.dto.UserReadDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

interface UserMappingService {

    fun mapCreateDtoToEntity(userDto: UserCreateDto): User

}

@Service
class UserMappingServiceImpl(
    private val userMapper: UserMapper,
    private val passwordEncoder: PasswordEncoder
) : UserMappingService {

    override fun mapCreateDtoToEntity(userDto: UserCreateDto): User {
        val user = userMapper.mapCreateDtoToEntity(userDto)
        user.password = passwordEncoder.encode(userDto.password)
        return user
    }
}

@Mapper
abstract class UserMapper {

    @Mapping(target = "password", ignore = true)
    abstract fun mapCreateDtoToEntity(user: UserCreateDto): User

    abstract fun entityToReadDto(user: User): UserReadDto
}





