package at.mindcloud.meetin.mappers

import at.mindcloud.meetin.entities.Project
import at.mindcloud.meetin.entities.dto.ProjectCreateDto
import at.mindcloud.meetin.entities.dto.ProjectUpdateDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings

@Mapper
abstract class ProjectMapper {

    @Mapping(target = "ownerId", source = "userId")
    abstract fun createDtoToEntity(project: ProjectCreateDto, userId: Long): Project


    @Mappings(
        Mapping(target = "ownerId", ignore = true)
    )
    abstract fun updateDtoToEntity(updateDto: ProjectUpdateDto, @MappingTarget entity: Project)

}
