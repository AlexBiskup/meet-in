package at.mindcloud.meetin.api

import at.mindcloud.meetin.entities.dto.UserCreateDto
import at.mindcloud.meetin.services.UserService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/signup")
class SignUpController(
    private val userService: UserService
) {

    @PostMapping
    fun signUp(@RequestBody user: UserCreateDto): Long {
        return userService.createUser(user)
    }

}
