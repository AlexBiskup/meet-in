package at.mindcloud.meetin.api

import at.mindcloud.meetin.entities.dto.AppointmentCreateDto
import at.mindcloud.meetin.entities.dto.AppointmentDetailsDto
import at.mindcloud.meetin.entities.dto.AppointmentReadDto
import at.mindcloud.meetin.entities.dto.AppointmentUpdateDto
import at.mindcloud.meetin.mappers.AppointmentMappingHelper
import at.mindcloud.meetin.services.AppointmentService
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/appointments")
class AppointmentsController(
    private val appointmentService: AppointmentService,
    private val appointmentMappingHelper: AppointmentMappingHelper
) {

    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: Long): AppointmentDetailsDto {
        val appointment = appointmentService.getById(id)
        return appointmentMappingHelper.entityToDetailsDto(appointment)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long) {
        appointmentService.delete(id)
    }

    @GetMapping
    fun getAppointments(@RequestParam("projectId") projectId: Long): List<AppointmentReadDto> {
        val appointments = appointmentService.getAppointmentsByProjectId(projectId)
        return appointments.map { appointmentMappingHelper.entityToReadDto(it) }
    }

    @PostMapping
    fun createAppointment(auth: Authentication, @RequestBody appointmentDto: AppointmentCreateDto): Long {
        // TODO forbid new appointments for not owners
        return appointmentService.createAppointment(appointmentDto)
    }

    @PutMapping("/{id}")
    fun updateAppointment(@RequestBody updatedAppointment: AppointmentUpdateDto, @PathVariable("id") id: Long) {
        appointmentService.update(id, updatedAppointment)
    }

}
