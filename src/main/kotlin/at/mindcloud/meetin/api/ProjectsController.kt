package at.mindcloud.meetin.api

import at.mindcloud.meetin.entities.dto.ProjectCreateDto
import at.mindcloud.meetin.entities.dto.ProjectReadDto
import at.mindcloud.meetin.entities.dto.ProjectUpdateDto
import at.mindcloud.meetin.services.ParticipantService
import at.mindcloud.meetin.services.ProjectMappingService
import at.mindcloud.meetin.services.ProjectService
import at.mindcloud.meetin.utilities.ControllerUtilities
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/projects")
class ProjectsController(
    private val projectService: ProjectService,
    private val participantService: ParticipantService,
    private val controllerUtilities: ControllerUtilities,
    private val projectMappingService: ProjectMappingService
) {

    @GetMapping
    fun getProjects(auth: Authentication): List<ProjectReadDto> {
        val userId = controllerUtilities.getUserIdByAuthentication(auth)
        val participants = participantService.getParticipantsByUserId(userId)
        return participants.map { projectMappingService.entityToDto(it) }
    }

    @GetMapping("/{projectId}")
    fun getProject(auth: Authentication, @PathVariable("projectId") projectId: Long): ProjectReadDto {
        val userId = controllerUtilities.getUserIdByAuthentication(auth)
        val participant = participantService.getParticipantByProjectIdAndUserId(projectId, userId)
        return projectMappingService.entityToDto(participant)
    }

    @PostMapping
    fun createProject(auth: Authentication, @RequestBody project: ProjectCreateDto): Long {
        val userId = controllerUtilities.getUserIdByAuthentication(auth)
        return projectService.createProject(project, userId)
    }

    @PutMapping("/{projectId}")
    fun updateProject(auth: Authentication, @RequestBody project: ProjectUpdateDto, @PathVariable("projectId") projectId: Long) {
        projectService.updateProject(projectId, project)
    }

}
