package at.mindcloud.meetin.api

import at.mindcloud.meetin.entities.dto.VoteReadDto
import at.mindcloud.meetin.entities.dto.VoteUpdateDto
import at.mindcloud.meetin.mappers.VoteMappingService
import at.mindcloud.meetin.services.ParticipantService
import at.mindcloud.meetin.services.VoteService
import at.mindcloud.meetin.utilities.ControllerUtilities
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/votes")
class VotesController(
    private val voteService: VoteService,
    private val controllerUtilities: ControllerUtilities,
    private val participantService: ParticipantService,
    private val voteMappingService: VoteMappingService
) {

    @GetMapping
    fun getVotesForParticipant(auth: Authentication, @RequestParam("projectId") projectId: Long): List<VoteReadDto> {
        val userId = controllerUtilities.getUserIdByAuthentication(auth)
        val participant = participantService.getParticipantByProjectIdAndUserId(projectId, userId)
        val votes = voteService.getVotesForParticipant(participant.id)
        return votes.map { voteMappingService.entityToReadDto(it) }
            .sortedBy { it.appointment.start }
    }

    @PutMapping("/{voteId}")
    fun updateVote(@PathVariable("voteId") voteId: Long, @RequestBody updatedVote: VoteUpdateDto) {
        voteService.updateVote(voteId, updatedVote)
    }

}
