package at.mindcloud.meetin.api

import at.mindcloud.meetin.entities.dto.ParticipantCreateDto
import at.mindcloud.meetin.entities.dto.ParticipantListDto
import at.mindcloud.meetin.mappers.ParticipantMappingService
import at.mindcloud.meetin.services.ParticipantService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/participants")
class ParticipantsController(
    private val participantService: ParticipantService,
    private val participantsMappingService: ParticipantMappingService
) {

    @PostMapping
    fun createParticipant(@RequestParam("projectId") projectId: Long, @RequestBody participantDto: ParticipantCreateDto): Long {
        return participantService.createParticipant(participantDto, projectId)
    }

    @GetMapping
    fun getParticipants(@RequestParam("projectId") projectId: Long): List<ParticipantListDto> {
        val participants = participantService.getParticipantsByProjectId(projectId)
        return participants.map { participantsMappingService.entityToListDto(it) }
    }

    @DeleteMapping("/{participantId}")
    fun deleteParticipant(@PathVariable("participantId") participantId: Long) {
        participantService.deleteParticipant(participantId)
    }

}
