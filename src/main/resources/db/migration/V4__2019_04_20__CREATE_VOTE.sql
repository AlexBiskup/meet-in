CREATE TABLE vote
(
    id             BIGINT(20)                                  NOT NULL AUTO_INCREMENT,
    appointment_id BIGINT(20)                                  NOT NULL,
    participant_id BIGINT(20)                                  NOT NULL,
    status         ENUM ('UNANSWERED', 'APPROVED', 'DECLINED') NOT NULL DEFAULT 'UNANSWERED',
    PRIMARY KEY (id),
    FOREIGN KEY (appointment_id) REFERENCES appointment (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (participant_id) REFERENCES participant (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT unique_for_appointment_and_participant UNIQUE (appointment_id, participant_id)
);
