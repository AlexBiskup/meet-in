CREATE TABLE appointment
(
    id         BIGINT(20)                                      NOT NULL AUTO_INCREMENT,
    project_id BIGINT(20)                                      NOT NULL,
    start      DATETIME                                        NOT NULL,
    end        DATETIME                                        NOT NULL,
    status     ENUM ('OPEN', 'RESERVED', 'FIXED', 'DISMISSED') NOT NULL,
    name       VARCHAR(255),
    PRIMARY KEY (id),
    FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT unique_start_end_for_project UNIQUE (project_id, start, end)
);
