CREATE TABLE project
(
    id       BIGINT(20)   NOT NULL AUTO_INCREMENT,
    name     VARCHAR(255) NOT NULL,
    owner_id BIGINT(20)   NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (owner_id) REFERENCES `user` (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT unique_name_for_user UNIQUE (name, owner_id)
);

CREATE TABLE participant
(
    id         BIGINT(20) NOT NULL AUTO_INCREMENT,
    user_id    BIGINT(20) NOT NULL,
    project_id BIGINT(20) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT user_once_per_project UNIQUE (user_id, project_id)
);
